package com.guosen.zebra.gateway.route.model;

/**
 * 子 URL 和方法名的映射
 */
public class SubUrlMethodMapping {

    /**
     * 子 URL
     */
    private String subUrl;

    /**
     * 方法名
     */
    private String method;

    public String getSubUrl() {
        return subUrl;
    }

    public void setSubUrl(String subUrl) {
        this.subUrl = subUrl;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
