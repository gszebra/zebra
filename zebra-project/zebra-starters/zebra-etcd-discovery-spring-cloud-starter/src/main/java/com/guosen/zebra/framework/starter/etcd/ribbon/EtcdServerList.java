package com.guosen.zebra.framework.starter.etcd.ribbon;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.guosen.zebra.framework.starter.etcd.properties.EtcdDiscoveryProperties;
import com.guosen.zebra.framework.starter.etcd.exception.EtcdDiscoveryException;
import com.guosen.zebra.framework.starter.etcd.util.EtcdClientUtil;
import com.guosen.zebra.framework.starter.etcd.util.JsonParser;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractServerList;
import io.etcd.jetcd.Client;
import io.etcd.jetcd.KeyValue;
import org.springframework.util.CollectionUtils;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class EtcdServerList extends AbstractServerList<EtcdServer> {

    private final EtcdDiscoveryProperties discoveryProperties;

    private String serviceId;

    public EtcdServerList(EtcdDiscoveryProperties discoveryProperties) {
        this.discoveryProperties = discoveryProperties;
    }

    @Override
    public List<EtcdServer> getInitialListOfServers() {
        return getServers();
    }

    @Override
    public List<EtcdServer> getUpdatedListOfServers() {
        return getServers();
    }

    @Override
    public void initWithNiwsConfig(IClientConfig clientConfig) {
        this.serviceId = clientConfig.getClientName();
    }

    private List<EtcdServer> getServers() {
        Client client = discoveryProperties.getClient();
        String prefix = discoveryProperties.getDiscoveryPrefix();
        String group = discoveryProperties.getGroup();
        String serviceIdPrefix = prefix + "/" + group + "/" + serviceId;

        List<KeyValue> kvs = EtcdClientUtil.getByPrefix(client, serviceIdPrefix, discoveryProperties.getTtl());
        if (CollectionUtils.isEmpty(kvs)) {
            return Collections.emptyList();
        }

        return kvs.stream()
                .map(keyValue -> toEtcdServer(serviceId, keyValue))
                .collect(Collectors.toList());
    }

    private EtcdServer toEtcdServer(String serviceId, KeyValue keyValue) {
        String valueStr = new String(keyValue.getValue().getBytes(), StandardCharsets.UTF_8);
        try {
            JsonNode value = JsonParser.objectMapper().readTree(valueStr);
            String host = value.get("host").asText();
            int port = value.get("port").asInt();
            return new EtcdServer(host, port);
        } catch (JsonProcessingException e) {
            throw new EtcdDiscoveryException(e);
        }
    }
}
