package com.guosen.zebra.framework.starter.etcd.exception;

import org.springframework.util.StringUtils;

/**
 * etcd服务发现异常
 */
public class EtcdDiscoveryException extends RuntimeException {

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    public EtcdDiscoveryException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public EtcdDiscoveryException(String message, Throwable cause) {
        super(message, cause);
    }

    public EtcdDiscoveryException(Exception e) {
        super(e);
    }

    @Override
    public String getMessage() {
        return StringUtils.hasLength(this.message) ? this.message : super.getMessage();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
