package com.guosen.zebra.framework.starter.captcha.autoconfigure;

import org.springframework.context.annotation.Configuration;

/**
 * 暂时没有需要配置的bean实例，anji-plus captcha代码中已处理
 */
@Configuration
public class CaptchaAutoConfiguration {
}
