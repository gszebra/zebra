package com.guosen.zebra.framework.starter.sms.constants;

/**
 * SMS异常提示语
 */
public class SmsExceptionMessage {

    private SmsExceptionMessage() {}

    public static final String SEND_SMS_FAILED = "短信发送失败！";
    public static final String QUERY_INTERNATIONAL_PREFIX_FAILED = "查询国际短信手机号码前缀列表失败！";
    public static final String QUERY_SMS_STATUS_FAILED = "查询短信发送状态失败！";
    public static final String QUERY_UP_SMS_FAILED = "查询号码近三日上行短信失败！";

    public static final String REQUEST_RESOLVE_FAILED = "请求参数转换异常！";
    public static final String RESPONSE_RESOLVE_FAILED = "响应数据转换异常！";
}
