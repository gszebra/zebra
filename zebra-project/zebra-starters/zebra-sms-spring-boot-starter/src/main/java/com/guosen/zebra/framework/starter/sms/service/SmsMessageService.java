package com.guosen.zebra.framework.starter.sms.service;

import com.guosen.zebra.framework.starter.sms.model.msg.SmsMsgRequest;
import com.guosen.zebra.framework.starter.sms.model.msg.SmsMsgResponse;

/**
 * SMS短信服务接口
 */
public interface SmsMessageService {

    /**
     * 通过手机号发送短信，不返回msgId
     */
    SmsMsgResponse sendMessage(SmsMsgRequest request);

    /**
     * 通过手机号发送短信，返回msgId
     */
    SmsMsgResponse sendMessageAndReturnId(SmsMsgRequest request);

    /**
     * 通过资金账号列表发送短信，返回msgId
     */
    SmsMsgResponse sendMessageByFundIds(SmsMsgRequest request);
}
