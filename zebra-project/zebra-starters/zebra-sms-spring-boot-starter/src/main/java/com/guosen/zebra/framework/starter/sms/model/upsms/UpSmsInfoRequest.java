package com.guosen.zebra.framework.starter.sms.model.upsms;

import lombok.Data;

/**
 * 根据手机号码查询近3日上行短信 - 请求参数
 */
@Data
public class UpSmsInfoRequest {

    /**
     * 手机号码
     */
    private String mobile;
}
