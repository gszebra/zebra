package com.guosen.zebra.framework.starter.sms.model.status;

import lombok.Data;

import java.util.List;

/**
 * SMS发送状态信息
 */
@Data
public class SmsStatusInfo {

    /**
     * 查询结果信息
     */
    private String msg;

    /**
     * 状态列表
     */
    private List<SmsStatus> list;
}
