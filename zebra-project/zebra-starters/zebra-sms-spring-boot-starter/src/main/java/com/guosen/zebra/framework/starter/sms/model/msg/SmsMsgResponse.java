package com.guosen.zebra.framework.starter.sms.model.msg;

import com.guosen.zebra.framework.starter.sms.model.SmsCommonResult;
import lombok.Data;

import java.util.List;

/**
 * 发送短信-返回参数
 */
@Data
public class SmsMsgResponse {

    /**
     * 公共出参
     */
    private List<SmsCommonResult> result;

    /**
     * 返回数据
     */
    private List<SmsMsgResponseData> data;
}
