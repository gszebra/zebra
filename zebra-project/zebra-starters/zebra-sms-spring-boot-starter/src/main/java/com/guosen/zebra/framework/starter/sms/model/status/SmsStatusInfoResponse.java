package com.guosen.zebra.framework.starter.sms.model.status;

import com.guosen.zebra.framework.starter.sms.model.SmsCommonResult;
import lombok.Data;

import java.util.List;

/**
 * SMS状态查询返回参数
 */
@Data
public class SmsStatusInfoResponse {

    /**
     * 公共出参
     */
    private List<SmsCommonResult> result;

    /**
     * 返回的发送状态信息
     */
    private List<SmsStatusInfo> data;
}
