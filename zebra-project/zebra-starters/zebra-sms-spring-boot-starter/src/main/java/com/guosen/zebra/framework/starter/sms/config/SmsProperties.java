package com.guosen.zebra.framework.starter.sms.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 短信平台配置类
 */
@Data
@ConfigurationProperties(prefix = "zebra.sms")
public class SmsProperties {

    /**
     * 短信平台地址
     */
    private String address;

    /**
     * 系统校验码
     */
    private String token;

    /**
     * 系统号
     */
    private String system;
}
