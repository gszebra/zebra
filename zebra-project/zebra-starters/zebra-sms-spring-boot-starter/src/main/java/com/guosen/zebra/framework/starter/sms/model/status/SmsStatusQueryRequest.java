package com.guosen.zebra.framework.starter.sms.model.status;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * SMS状态查询请求参数
 */
@Data
public class SmsStatusQueryRequest {

    @JsonProperty("smsid")
    private String smsId;
}
