package com.guosen.zebra.framework.starter.sms.model.international;

import com.guosen.zebra.framework.starter.sms.model.SmsCommonResult;
import lombok.Data;

import java.util.List;

/**
 * 查询国际短信手机号码前缀 - 返回参数
 */
@Data
public class SmsInternationalPrefixResponse {

    private List<SmsCommonResult> result;

    private List<InternationalPrefixInfo> data;
}
