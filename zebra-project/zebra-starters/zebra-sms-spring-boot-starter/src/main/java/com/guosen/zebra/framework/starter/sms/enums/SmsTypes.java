package com.guosen.zebra.framework.starter.sms.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 短信类型
 */
@Getter
@AllArgsConstructor
public enum SmsTypes {

    NORMAL("1", "普通"),
    SEND_DIRECTLY("2", "不检查浪费短信，直接发送"),
    ;

    private final String type;
    private final String description;
}
