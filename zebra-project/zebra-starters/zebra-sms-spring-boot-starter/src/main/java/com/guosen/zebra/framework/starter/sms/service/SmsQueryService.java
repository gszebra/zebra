package com.guosen.zebra.framework.starter.sms.service;

import com.guosen.zebra.framework.starter.sms.model.international.SmsInternationalPrefixResponse;
import com.guosen.zebra.framework.starter.sms.model.status.SmsStatusInfoResponse;
import com.guosen.zebra.framework.starter.sms.model.upsms.UpSmsInfoResponse;

/**
 * SMS查询服务接口
 */
public interface SmsQueryService {

    /**
     * 查询国际短信手机号码前缀
     */
    SmsInternationalPrefixResponse queryInternationalPrefix();

    /**
     * 根据短信id查询短信发送状态
     * @param smsId 短信ID
     */
    SmsStatusInfoResponse querySmsStatusInfo(String smsId);

    /**
     *根据手机号码查询近三日上行短信
     * @param mobile 手机号码
     */
    UpSmsInfoResponse queryUpSmsInfo(String mobile);
}
