package com.guosen.zebra.framework.starter.sms.autoconfigure;

import com.guosen.zebra.framework.starter.sms.config.SmsProperties;
import com.guosen.zebra.framework.starter.sms.service.SmsMessageService;
import com.guosen.zebra.framework.starter.sms.service.SmsQueryService;
import com.guosen.zebra.framework.starter.sms.service.impl.SmsMessageServiceImpl;
import com.guosen.zebra.framework.starter.sms.service.impl.SmsQueryServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(SmsProperties.class)
public class SmsAutoConfiguration {

    @Bean
    public SmsMessageService smsMessageService(SmsProperties smsProperties) {
        return new SmsMessageServiceImpl(smsProperties);
    }

    @Bean
    public SmsQueryService smsQueryService(SmsProperties smsProperties) {
        return new SmsQueryServiceImpl(smsProperties);
    }
}
