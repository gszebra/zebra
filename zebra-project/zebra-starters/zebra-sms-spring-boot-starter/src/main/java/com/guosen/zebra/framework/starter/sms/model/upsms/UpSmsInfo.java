package com.guosen.zebra.framework.starter.sms.model.upsms;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 上行短信信息
 */
@Data
public class UpSmsInfo {

    /**
     * 查询结果信息
     */
    private String msg;

    /**
     * 短信内容列表
     */
    private List<UpSms> list;
}
