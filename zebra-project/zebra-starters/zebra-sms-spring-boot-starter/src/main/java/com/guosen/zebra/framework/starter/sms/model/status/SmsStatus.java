package com.guosen.zebra.framework.starter.sms.model.status;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Sms状态
 */
@Data
public class SmsStatus {

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 产品ID
     */
    private String product;

    /**
     * 返回码，0-发送成功，其他数值-失败
     */
    @JsonProperty("scode")
    private String sCode;

    /**
     * 返回信息，如果sCode为失败，该字段为失败原因
     */
    @JsonProperty("smsg")
    private String sMsg;

    /**
     * 短信ID
     */
    @JsonProperty("smsid")
    private String smsId;

    /**
     * 短信发送时间
     */
    @JsonProperty("createtime")
    private String createTime;
}
