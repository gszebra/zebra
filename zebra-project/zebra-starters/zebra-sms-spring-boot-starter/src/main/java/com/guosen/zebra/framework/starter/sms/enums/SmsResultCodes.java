package com.guosen.zebra.framework.starter.sms.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * SMS返回码
 */
@Getter
@AllArgsConstructor
public enum SmsResultCodes {

    SUCCESS("0", "成功"),
    ;

    private final String code;
    private final String description;
}
