package com.guosen.zebra.framework.starter.sms.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * SMS通道
 */
@Getter
@AllArgsConstructor
public enum SmsChannels {

    DEFAULT("1", "默认"),
    INTERNATIONAL("n", "国际短信"),
    ;

    private final String channel;
    private final String description;
}
