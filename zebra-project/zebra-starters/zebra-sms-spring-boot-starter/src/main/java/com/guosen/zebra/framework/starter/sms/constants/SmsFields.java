package com.guosen.zebra.framework.starter.sms.constants;

/**
 * 参数字段名常量
 */
public class SmsFields {

    private SmsFields() {}

    public static final String TOKEN = "token";

    public static final String SMS_ID = "smsid";

    public static final String MOBILE = "mobile";

    public static final String SYSTEM = "system";

}
