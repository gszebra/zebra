package com.guosen.zebra.framework.starter.sms.model;

import lombok.Data;

/**
 * SMS-公共出参
 */
@Data
public class SmsCommonResult {

    /**
     * 返回消息，当 code不为 0时，msg为错误信息
     */
    private String msg;
    /**
     * 返回码，0-成功，其他失败
     */
    private String code;

    private String hash;
}
