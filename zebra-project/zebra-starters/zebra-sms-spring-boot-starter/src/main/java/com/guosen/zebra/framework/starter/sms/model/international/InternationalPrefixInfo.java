package com.guosen.zebra.framework.starter.sms.model.international;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 国际短信手机号码前缀信息
 */
@Data
public class InternationalPrefixInfo {

    /**
     * 序号
     */
    private Integer sn;

    /**
     * 中文国家名称
     */
    @JsonProperty("countrycn")
    private String countryCN;

    /**
     * 英文国家名称
     */
    @JsonProperty("countryen")
    private String countryEN;

    /**
     * 国际号码前缀
     */
    @JsonProperty("phonepre")
    private String phonePre;

    /**
     * 中文拼音
     */
    @JsonProperty("pinyin")
    private String pinYin;

    /**
     * 国家简称
     */
    @JsonProperty("shortcode")
    private String shortCode;
}
