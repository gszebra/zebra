package com.guosen.zebra.framework.starter.sms.model.upsms;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 上行短信内容
 */
@Data
public class UpSms {

    /**
     * 上行内容
     */
    private String content;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 短信接收时间
     */
    private String receiveTime;

    /**
     * 上行通道号
     */
    @JsonProperty("spcode")
    private String spCode;
}
