package com.guosen.zebra.framework.starter.sms.model.upsms;

import com.guosen.zebra.framework.starter.sms.model.SmsCommonResult;
import lombok.Data;

import java.util.List;

/**
 * 根据手机号码查询近3日上行短信 - 返回参数
 */
@Data
public class UpSmsInfoResponse {

    private List<SmsCommonResult> result;

    private List<UpSmsInfo> data;
}
