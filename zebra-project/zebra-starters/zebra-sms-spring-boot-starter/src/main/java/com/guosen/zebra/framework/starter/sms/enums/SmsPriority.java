package com.guosen.zebra.framework.starter.sms.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 短信优先级
 */
@Getter
@AllArgsConstructor
public enum SmsPriority {

    HIGHEST("1", "最高"),
    HIGH("2", "高"),
    NORMAL("3", "普通"),
    ;
    private final String priority;
    private final String description;
}
