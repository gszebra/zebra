package com.guosen.zebra.framework.starter.sms.model.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 发送短信-返回数据部分
 */
@Data
public class SmsMsgResponseData {

    /**
     * 短信ID
     */
    @JsonProperty("smsid")
    private String smsId;

    /**
     * 消息
     */
    private String msg;
}
