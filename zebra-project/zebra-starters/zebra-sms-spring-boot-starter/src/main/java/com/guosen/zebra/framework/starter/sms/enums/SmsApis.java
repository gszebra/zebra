package com.guosen.zebra.framework.starter.sms.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 短信平台接口
 */
@Getter
@AllArgsConstructor
public enum SmsApis {

    SEND_MESSAGE("/smsservice/sms/send", "发送短信接口，不返回短信id"),
    SEND_MESSAGE_AND_RETURN_ID("/smsservice/smsstatus/sendsmsandgetid", "发送短信接口（返回短信id）"),
    GET_INTERNATIONAL_PREFIX_LIST("/smsservice/worldsms/getcountryphonecodelist", "查询国际短信手机号码前缀列表"),
    GET_SMS_STATUS("/smsservice/smsstatus/getsmsstatus", "根据短信id查询短信发送状态"),
    SEND_MESSAGE_BY_FUNDIDS("/smsservice/fundidsms/sendsmsbyfundids", "通过资金账号列表发送短信接口"),
    GET_UP_SMS_BY_MOBILE("/smsservice/sms/getsmsinfoinbymobile", "根据手机号查询最近3日的上行短信。超过3天的上行短信已归档，所以查询不到历史上行短信，只能查询到最近3天的短信上行记录。"),

    ;

    private final String api;
    private final String description;
}
