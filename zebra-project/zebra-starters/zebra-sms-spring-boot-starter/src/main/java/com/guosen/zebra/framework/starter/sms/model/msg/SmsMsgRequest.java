package com.guosen.zebra.framework.starter.sms.model.msg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 发送短信-请求参数
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsMsgRequest {

    /**
     * 手机号列表，多个手机号以分号隔开
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mobiles;

    /**
     * 多个资金账号以分号隔开，最大100个资金账号
     */
    @JsonProperty("fundids")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fundIds;

    /**
     * 内容，建议在70字以内，超过70字将分多条短信发送。最大不超过200字。
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String content;

    /**
     * 优先级
     * {@link com.guosen.zebra.framework.starter.sms.enums.SmsPriority}
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String priority;

    /**
     * 类型
     * {@link com.guosen.zebra.framework.starter.sms.enums.SmsTypes}
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;

    /**
     * 子号
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("subcode")
    private String subCode;

    /**
     * 系统号
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String system;

    /**
     * 通道，国内短信目前无需传，系统自动选择。
     * {@link com.guosen.zebra.framework.starter.sms.enums.SmsChannels}
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String channel;

    /**
     * 发送者
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sender;

    /**
     * 发送者营业部
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("senderbranch")
    private String senderBranch;

    /**
     * 计费营业部
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("chargebranch")
    private String chargeBranch;

    /**
     * 计费用户
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("chargeuser")
    private String chargeUser;

    /**
     * 产品ID，如果是短信口令传 password，如果需要不校验重复短信，传password，其他情况不传
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String product;

    /**
     * 审核人
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String reviewer;
}
