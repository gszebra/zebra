package com.guosen.zebra.framework.starter.sms.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.guosen.zebra.framework.starter.sms.config.SmsProperties;
import com.guosen.zebra.framework.starter.sms.internal.SmsHttpClient;
import com.guosen.zebra.framework.starter.sms.model.msg.SmsMsgRequest;
import com.guosen.zebra.framework.starter.sms.model.msg.SmsMsgResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

/**
 * SMS短信服务单测
 */
@ExtendWith(MockitoExtension.class)
class SmsMessageServiceImplTest {

    private static final String BASE_URL = "http://127.0.0.1:8080";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Mock
    private SmsProperties smsProperties;

    @InjectMocks
    private SmsMessageServiceImpl smsMessageService;

    @Test
    void testSendMessage() {
        SmsMsgRequest smsMsgRequest = SmsMsgRequest.builder()
                .content("sms content")
                .mobiles("1333333333")
                .build();

        try (MockedStatic<SmsHttpClient> smsHttpClientMock = mockStatic(SmsHttpClient.class)) {
            smsHttpClientMock.when(() -> SmsHttpClient.post(anyString(), any()))
                    .thenReturn(getMockResult());
            when(smsProperties.getAddress()).thenReturn(BASE_URL);
            SmsMsgResponse smsMsgResponse = smsMessageService.sendMessage(smsMsgRequest);
            assertThat(smsMsgResponse.getResult().size(), is(0));
        }
    }

    private static JsonNode getMockResult() {
        String jsonString = "{\"result\":[],\"data\":[]}";
        try {
            ObjectMapper objectMapper = new ObjectMapper(); // 手动声明类型
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return objectMapper.readTree(jsonString);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
