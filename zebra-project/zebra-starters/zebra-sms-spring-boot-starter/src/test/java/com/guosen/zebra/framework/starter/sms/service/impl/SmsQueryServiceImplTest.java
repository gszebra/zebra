package com.guosen.zebra.framework.starter.sms.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.guosen.zebra.framework.starter.sms.config.SmsProperties;
import com.guosen.zebra.framework.starter.sms.exception.SmsException;
import com.guosen.zebra.framework.starter.sms.internal.SmsHttpClient;
import com.guosen.zebra.framework.starter.sms.model.international.SmsInternationalPrefixResponse;
import com.guosen.zebra.framework.starter.sms.model.status.SmsStatusInfoResponse;
import com.guosen.zebra.framework.starter.sms.model.upsms.UpSmsInfoResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static com.guosen.zebra.framework.starter.sms.constants.SmsFields.TOKEN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

/**
 * SMS查询服务单测
 */
@ExtendWith(MockitoExtension.class)
class SmsQueryServiceImplTest {

    private static final String MOBILE = "13333333333";
    private static final String BASE_URL = "http://127.0.0.1:8080";

    @Mock
    private SmsProperties smsProperties;

    @InjectMocks
    private SmsQueryServiceImpl smsQueryService;

    @Test
    void testQueryInternationalPrefix() {
        try (MockedStatic<SmsHttpClient> smsHttpClientMock = mockStatic(SmsHttpClient.class)) {
            smsHttpClientMock.when(() -> SmsHttpClient.post(anyString(), any()))
                    .thenReturn(getMockResult());
            when(smsProperties.getAddress()).thenReturn(BASE_URL);
            when(smsProperties.getToken()).thenReturn(TOKEN);
            SmsInternationalPrefixResponse response = smsQueryService.queryInternationalPrefix();
            assertThat(response.getResult().size(), is(0));
        }
    }

    @Test
    void testQueryInternationalPrefixWithException() {
        try (MockedStatic<SmsHttpClient> smsHttpClientMock = mockStatic(SmsHttpClient.class)) {
            smsHttpClientMock.when(() -> SmsHttpClient.post(anyString(), any()))
                    .thenThrow(new SmsException(new IOException("sms exception！")));
            when(smsProperties.getAddress()).thenReturn(BASE_URL);
            when(smsProperties.getToken()).thenReturn(TOKEN);
            assertThrows(SmsException.class, () -> smsQueryService.queryInternationalPrefix());
        }
    }

    @Test
    void testQuerySmsStatusInfo() {
        try (MockedStatic<SmsHttpClient> smsHttpClientMock = mockStatic(SmsHttpClient.class)) {
            smsHttpClientMock.when(() -> SmsHttpClient.post(anyString(), any()))
                    .thenReturn(getMockResult());
            when(smsProperties.getAddress()).thenReturn(BASE_URL);
            when(smsProperties.getToken()).thenReturn(TOKEN);
            SmsStatusInfoResponse response = smsQueryService.querySmsStatusInfo(MOBILE);
            assertThat(response.getResult().size(), is(0));
        }
    }

    @Test
    void testQuerySmsStatusInfoWithException() {
        try (MockedStatic<SmsHttpClient> smsHttpClientMock = mockStatic(SmsHttpClient.class)) {
            smsHttpClientMock.when(() -> SmsHttpClient.post(anyString(), any()))
                    .thenThrow(new SmsException(new IOException("sms exception！")));
            when(smsProperties.getAddress()).thenReturn(BASE_URL);
            when(smsProperties.getToken()).thenReturn(TOKEN);
            assertThrows(SmsException.class, () -> smsQueryService.querySmsStatusInfo(MOBILE));
        }
    }

    @Test
    void testQueryUpSmsInfo() {
        try (MockedStatic<SmsHttpClient> smsHttpClientMock = mockStatic(SmsHttpClient.class)) {
            smsHttpClientMock.when(() -> SmsHttpClient.post(anyString(), any()))
                    .thenReturn(getMockResult());
            when(smsProperties.getAddress()).thenReturn(BASE_URL);
            when(smsProperties.getToken()).thenReturn(TOKEN);
            UpSmsInfoResponse response = smsQueryService.queryUpSmsInfo(MOBILE);
            assertThat(response.getResult().size(), is(0));
        }
    }

    @Test
    void testQueryUpSmsInfoWithException() {
        try (MockedStatic<SmsHttpClient> smsHttpClientMock = mockStatic(SmsHttpClient.class)) {
            smsHttpClientMock.when(() -> SmsHttpClient.post(anyString(), any()))
                    .thenThrow(new SmsException(new IOException("sms exception！")));
            when(smsProperties.getAddress()).thenReturn(BASE_URL);
            when(smsProperties.getToken()).thenReturn(TOKEN);
            assertThrows(SmsException.class, () -> smsQueryService.queryUpSmsInfo(MOBILE));
        }
    }

    private static JsonNode getMockResult() {
        String jsonString = "{\"result\":[],\"data\":[]}";

        try {
            ObjectMapper objectMapper = new ObjectMapper(); // 手动声明类型
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return objectMapper.readTree(jsonString);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
