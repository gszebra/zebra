package com.guosen.zebra.framework.starter.s3.exception;

/**
 * S3 异常
 */
public class S3Exception extends RuntimeException {
    public S3Exception(String errorMessage, Exception e) {
        super(errorMessage, e);
    }

    public S3Exception(String errorMessage) {
        super(errorMessage);
    }

    public S3Exception(Exception e) {
        super(e);
    }
}