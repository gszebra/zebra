package com.guosen.zebra.framework.starter.s3.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * S3 配置类
 */
@Data
@ConfigurationProperties(prefix = "zebra.s3")
public class S3Properties {

    /**
     * S3 AK
     */
    private String ak;

    /**
     * S3 SK
     */
    private String sk;

    /**
     * S3 地址
     */
    private String endpoint;

    /**
     * 默认 S3 bucket
     */
    private String bucket;
}
