package com.guosen.zebra.framework.starter.s3.service;

import com.guosen.zebra.framework.starter.s3.exception.S3Exception;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Zebra S3 服务接口<br>
 * 本接口仅封装了常用的 S3 操作，使用配置指定了默认bucket，如有其他需求，可使用原生 com.amazonaws.services.s3.AmazonS3
 */
public interface ZebraS3Service {

    /**
     * 上传指定文件到S3
     * @param key   文件S3名称
     * @param file  待上传的文件
     * @throws S3Exception  上传异常
     */
    void upload(String key, File file) throws S3Exception;

    /**
     * 将指定文本内容作为文件上传到S3
     * @param key       文件S3名称
     * @param content   文本内容
     * @throws S3Exception  上传异常
     */
    void upload(String key, String content) throws S3Exception;


    /**
     * 上传文件
     * @param key   文件S3名称
     * @param multipartFile controller中上传文件对象
     */
    void upload(String key, MultipartFile multipartFile);

    /**
     * 上传文件
     * @param key   文件S3名称
     * @param inputStream   文件输入流程
     * @param contentLength  文件长度
     * @throws S3Exception 上传失败
     */
    void upload(String key, InputStream inputStream, long contentLength) throws S3Exception;

    /**
     * 下载文件
     * @param key       文件S3名称
     * @param outputStream  S3文件要输出的流
     * @throws S3Exception 下载失败
     */
    void download(String key, OutputStream outputStream) throws S3Exception;


    /**
     * 将S3文件读取为string
     * @param key 文件S3名称
     * @return S3 string 内容
     * @throws S3Exception 下载失败
     */
    String downloadAsString(String key) throws S3Exception ;

    /**
     * 删除文件
     * @param key   文件S3名称
     * @throws S3Exception  删除失败
     */
    void delete(String key) throws S3Exception;

    /**
     * 批量删除文件
     * @param keys  文件S3名称列表
     * @throws S3Exception  删除失败
     */
    void batchDelete(List<String> keys) throws S3Exception;

    /**
     * 复制文件
     * @param srcKey    源s3文件名称
     * @param destKey   目标s3文件名称
     */
    void copy(String srcKey, String destKey) throws S3Exception;

    /**
     * 文件是否存在
     * @param key   S3文件名称
     * @return  true : 存在；false：不存在
     * @throws S3Exception 查询失败
     */
    boolean exist(String key) throws S3Exception;

    /**
     * 返回给定前缀的所有文件路径
     */
    List<String> listKeys(String prefix) throws S3Exception;
}
