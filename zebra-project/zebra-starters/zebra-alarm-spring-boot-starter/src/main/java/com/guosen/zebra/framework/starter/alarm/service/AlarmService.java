package com.guosen.zebra.framework.starter.alarm.service;

import com.guosen.zebra.framework.starter.alarm.model.AlarmEvent;

/**
 * 微观告警平台服务接口
 */
public interface AlarmService {

    /**
     * 发布事件
     * @param event 事件参数
     */
    void publish(AlarmEvent event);
}
