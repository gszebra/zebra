package com.guosen.zebra.framework.starter.alarm.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 告警级别枚举
 */
@Getter
@AllArgsConstructor
public enum AlarmSeverityLevel {

    UNCLASSIFIED("0", "未分类"),
    EXTREME_HIGH("1", "极高"),
    HIGH("2", "高"),
    MEDIUM("3", "中"),
    LOW("4", "低"),

    ;
    private String level;
    private String description;
}
