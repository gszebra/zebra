package com.guosen.zebra.framework.starter.alarm.autoconfigure;

import com.guosen.zebra.framework.starter.alarm.properties.AlarmProperties;
import com.guosen.zebra.framework.starter.alarm.service.AlarmService;
import com.guosen.zebra.framework.starter.alarm.service.impl.AlarmServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(AlarmProperties.class)
public class AlarmAutoConfiguration {

    @Bean
    public AlarmService alarmService(AlarmProperties alarmProperties) {
        return new AlarmServiceImpl(alarmProperties);
    }
}
