package com.guosen.zebra.framework.starter.alarm.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 告警事件类型
 */
@Getter
@AllArgsConstructor
public enum AlarmEventTypes {

    ALARM("1", "告警事件"),
    RECOVERY("2", "恢复事件"),

    ;

    private String type;
    private String description;
}
