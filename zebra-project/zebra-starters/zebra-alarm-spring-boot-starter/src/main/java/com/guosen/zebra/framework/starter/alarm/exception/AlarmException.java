package com.guosen.zebra.framework.starter.alarm.exception;

import org.apache.commons.lang3.StringUtils;

/**
 * 告警平台接口调用异常
 */
public class AlarmException extends RuntimeException {

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    public AlarmException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public AlarmException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlarmException(Exception e) {
        super(e);
    }

    @Override
    public String getMessage() {
        return StringUtils.isNotEmpty(this.message) ? this.message : super.getMessage();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
