package com.guosen.zebra.framework.starter.alarm.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "zebra.alarm")
public class AlarmProperties {

    /**
     * 告警平台地址
     */
    private String address;
}
