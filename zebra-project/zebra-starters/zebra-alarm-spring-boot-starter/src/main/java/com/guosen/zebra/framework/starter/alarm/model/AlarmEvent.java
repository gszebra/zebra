package com.guosen.zebra.framework.starter.alarm.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * 告警事件
 */
@Data
@Builder
public class AlarmEvent {

    /**
     * 告警事件标题
     */
    private String eventName;

    /**
     * 告警事件描述
     */
    private String summary;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 主机名称
     */
    @JsonProperty("HOSTNAME")
    private String hostName;

    /**
     * 组件名称
     */
    @JsonProperty("COMPONENT")
    private String component;

    /**
     * 告警事件发生时间
     */
    private String eventTime;

    /**
     * 事件优先级
     *  {@link com.guosen.zebra.framework.starter.alarm.enums.AlarmSeverityLevel}
     */
    private String severity;

    /**
     * 事件的类型
     * {@link com.guosen.zebra.framework.starter.alarm.enums.AlarmEventTypes}
     */
    private String type;

    /**
     * 监控工具
     */
    private String agent;
}
