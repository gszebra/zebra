package com.guosen.zebra.framework.starter.alarm.service.impl;

import com.guosen.zebra.framework.starter.alarm.properties.AlarmProperties;
import com.guosen.zebra.framework.starter.alarm.exception.AlarmException;
import com.guosen.zebra.framework.starter.alarm.internal.AlarmHttpClient;
import com.guosen.zebra.framework.starter.alarm.model.AlarmEvent;
import com.guosen.zebra.framework.starter.alarm.service.AlarmService;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * 微观告警平台服务实现
 */
@Slf4j
public class AlarmServiceImpl implements AlarmService {

    private final AlarmProperties alarmProperties;

    public AlarmServiceImpl(AlarmProperties alarmProperties) {
        this.alarmProperties = alarmProperties;
    }

    @Override
    public void publish(AlarmEvent event) {
        String alarmAddress = alarmProperties.getAddress();
        try {
            // 目前接口只要地址正确，不管发送什么都返回ok（即便参数都不传）
            // 默认每次都发送成功。
            Object result = AlarmHttpClient.postAlarmEvent(alarmAddress, new HashMap<>(), event);
            log.info("[Alarm] Alarm event has been pushed, result : {}", result);
        } catch (AlarmException e) {
            log.error("[Alarm] Alarm event push failed, event:{}, error message:{}", event, e.getMessage(), e);
            throw e;
        }
    }
}
