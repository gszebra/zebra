package com.guosen.zebra.framework.starter.alarm.service.impl;

import com.guosen.zebra.framework.starter.alarm.exception.AlarmException;
import com.guosen.zebra.framework.starter.alarm.internal.AlarmHttpClient;
import com.guosen.zebra.framework.starter.alarm.model.AlarmEvent;
import com.guosen.zebra.framework.starter.alarm.properties.AlarmProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

/**
 * 微观告警平台单测
 */
@ExtendWith(MockitoExtension.class)
class AlarmServiceImplTest {

    private static final String BASE_URL = "http://127.0.0.1:8080";

    @Mock
    private AlarmProperties alarmProperties;

    @InjectMocks
    private AlarmServiceImpl alarmService;

    @Test
    void testPublish() {
        AlarmEvent event = AlarmEvent.builder().build();
        HashMap<String, String> map = new HashMap<>();

        when(alarmProperties.getAddress()).thenReturn(BASE_URL);
        try (MockedStatic<AlarmHttpClient> mock = mockStatic(AlarmHttpClient.class)) {
            mock.when(() -> AlarmHttpClient.postAlarmEvent(BASE_URL, map, event))
                    .thenReturn(null);
            assertDoesNotThrow(() -> alarmService.publish(event));
        }
    }

    @Test
    void testPublishWithException() {
        AlarmEvent event = AlarmEvent.builder().build();
        HashMap<String, String> map = new HashMap<>();

        when(alarmProperties.getAddress()).thenReturn(BASE_URL);
        try (MockedStatic<AlarmHttpClient> mock = mockStatic(AlarmHttpClient.class)) {
            mock.when(() -> AlarmHttpClient.postAlarmEvent(BASE_URL, map, event))
                    .thenThrow(new AlarmException(new IOException("alarm exception")));

            assertThrows(AlarmException.class, () -> alarmService.publish(event));
        }
    }
}
