package com.guosen.zebra.framework.starter.sso.service;


import com.guosen.zebra.framework.starter.sso.vo.SsoLoginVO;
import org.springframework.web.servlet.function.ServerRequest;

/**
 * SSO支持服务，提供辅助操作
 */
public interface SsoSupportService {

    /**
     * 登录成功后重定向，优先取入参returnUrl，若为空则跳转到homePage，homePage默认为/
     */
    String redirectPage(String returnUrl);

    /**
     * 返回错误页
     */
    String errorPage(String errorMsg, ServerRequest request);

    /**
     * 返回自动提交表单页
     */
    String autoPostLoginPage(SsoLoginVO ssoLoginVO, ServerRequest request);

    /**
     * 返回登录成功页
     */
    String loginSuccessPage();
}
