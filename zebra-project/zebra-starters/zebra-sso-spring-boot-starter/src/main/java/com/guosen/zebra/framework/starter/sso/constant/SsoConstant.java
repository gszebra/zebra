package com.guosen.zebra.framework.starter.sso.constant;


public final class SsoConstant {
    private SsoConstant(){}

    public static final String SUCCESS = "0";

    public static final String IA_SID = "IASID";

    public static final String RETURN_URL = "ReturnURL";
    public static final String USER_ACCOUNT = "UserAccount";
    public static final String RESULT = "Result";
    public static final String ERROR_DESCRIPTION = "ErrorDescription";
    public static final String AUTHENTICATOR = "Authenticator";
    public static final String TIME_STAMP = "TimeStamp";

    public static final String EAC_ADDRESS = "EacAddress";

    public static final String SUCCESS_TEMPLATE = "loginSuccess";

    public static final String ERROR_TEMPLATE = "errorPage";

    public static final String AUTO_POST_LOGIN_TEMPLATE = "autoPostLogin";

    public static final String REDIRECT_PREFIX = "redirect:";
}
