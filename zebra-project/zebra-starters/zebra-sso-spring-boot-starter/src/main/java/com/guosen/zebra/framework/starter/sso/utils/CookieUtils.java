package com.guosen.zebra.framework.starter.sso.utils;

import org.springframework.web.servlet.function.ServerRequest;

import javax.servlet.http.Cookie;

public abstract class CookieUtils {

    /**
     * 根据名字获取cookie
     * @param request request
     * @param name    cookie名称
     * @return cookie
     */
    public static Cookie getCookieByName(ServerRequest request, String name) {
        return request.cookies().getFirst(name);}
}
