package com.guosen.zebra.framework.starter.sso.service.impl;

import com.guosen.zebra.framework.starter.sso.dto.EacResponse;
import com.guosen.zebra.framework.starter.sso.service.SsoCallBackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

/**
 * 默认sso回调服务
 */
@Slf4j
public class DefaultSsoCallBackService implements SsoCallBackService {

    @Override
    public String handleLoginSuccess(ServerRequest request, ServerResponse.BodyBuilder response, EacResponse eacResponse) {
        log.info("[sso] default login success ....");
        return null;
    }

    @Override
    public void handleSsoValidateFailed(ServerRequest request, ServerResponse.BodyBuilder response, EacResponse eacResponse) {
        log.error("[sso] default validate failed ....");
    }
}
