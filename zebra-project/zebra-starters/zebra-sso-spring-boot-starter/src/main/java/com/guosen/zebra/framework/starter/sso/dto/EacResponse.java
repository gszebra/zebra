package com.guosen.zebra.framework.starter.sso.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EacResponse {
    /**
     * ID
     */
    @JsonProperty("IASID")
    private String iasId;

    /**
     * 时间
     */
    @JsonProperty("TimeStamp")
    private String timeStamp;

    /**
     * 账号
     */
    @JsonProperty("UserAccount")
    private String userAccount;

    /**
     * 签名
     */
    @JsonProperty("Authenticator")
    private String authenticator;

    /**
     * 返回结果
     */
    @JsonProperty("Result")
    private String result;

    /**
     * 错误描述
     */
    @JsonProperty("ErrorDescription")
    private String errorDescription;
}
