package com.guosen.zebra.framework.starter.sso.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * 提交sso post 表单参数
 */
@Data
@Builder
public class SsoLoginVO {
    /**
     * ID
     */
    @JsonProperty("IASID")
    private String iasId;

    /**
     * sso请求地址
     */
    private String eacAddress;

    /**
     * 时间
     */
    @JsonProperty("TimeStamp")
    private String timeStamp;

    /**
     * 回调地址
     */
    @JsonProperty("ReturnURL")
    private String returnUrl;

    /**
     * 签名
     */
    @JsonProperty("Authenticator")
    private String ssoSignature;

    /**
     * 其它用户自定义属性
     */
    private Map<String, Object> map;
}
