package com.guosen.zebra.framework.starter.sso.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.guosen.zebra.framework.starter.sso.properties.SsoProperties.PREFIX;

@ConfigurationProperties(PREFIX)
@Data
public class SsoProperties {

    public static final String PREFIX = "zebra.sso";

    /**
     * SSO分配的ID
     */
    private String iasId;

    /**
     * SSO分配的key
     */
    private String iasKey;

    /**
     * 系统前置URL
     */
    private String baseUrl;

    /**
     * SSO系统登录指向地址
     */
    private String eacAddress;

    /**
     * 主页，登录成功后的默认重定向地址
     */
    private String homePage = "/";

    /**
     * sso认证过期时间，默认15，单位分钟
     */
    private Integer authExpiration = 15;

    /**
     * sso登录请求路径，默认为/api/sso/ssoLoginPage
     */
    private String ssoLoginPath = "/api/sso/ssoLoginPage";

    /**
     * sso返回请求路径，默认为/api/sso/ssoReturnUrl
     */
    private String ssoReturnPath = "/api/sso/ssoReturnUrl";

    /**
     * 模板路径，默认为defaultSsoTemplates
     */
    private String templatesPath = "defaultSsoTemplates";
}
