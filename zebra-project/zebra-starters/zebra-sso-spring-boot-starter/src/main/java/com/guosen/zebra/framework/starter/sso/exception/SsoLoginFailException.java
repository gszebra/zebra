package com.guosen.zebra.framework.starter.sso.exception;

import lombok.Data;

@Data
public class SsoLoginFailException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public SsoLoginFailException() {
        super();
    }
    public SsoLoginFailException(String message) {
        super(message);
    }
}
