package com.guosen.zebra.framework.starter.sso.service;

import com.guosen.zebra.framework.starter.sso.dto.EacResponse;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

/**
 * SSO回调服务，支持个性化登录成功后的逻辑
 */
public interface SsoCallBackService {

    /**
     * sso登录成功后回调
     * 返回null或“”表示调用正常
     */
    String handleLoginSuccess(ServerRequest request, ServerResponse.BodyBuilder response, EacResponse eacResponse);

    /**
     * sso登录校验失败时回调
     */
    void handleSsoValidateFailed(ServerRequest request, ServerResponse.BodyBuilder response, EacResponse eacResponse);
}
