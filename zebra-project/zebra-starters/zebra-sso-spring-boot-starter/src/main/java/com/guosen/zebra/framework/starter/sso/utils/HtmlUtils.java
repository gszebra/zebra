package com.guosen.zebra.framework.starter.sso.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Slf4j
public final class HtmlUtils {

    private HtmlUtils(){}

    public static void writeHtml(HttpServletResponse response, String html) {
        response.setHeader("Content-Type","text/html;charset=UTF-8");

        try (PrintWriter pw = response.getWriter()) {
            pw.write(html);
            pw.flush();
        } catch (IOException e) {
            log.error("Failed to write html", e);
        }
    }
}
