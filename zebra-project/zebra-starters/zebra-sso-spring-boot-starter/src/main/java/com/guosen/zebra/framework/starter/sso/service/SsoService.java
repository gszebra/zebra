package com.guosen.zebra.framework.starter.sso.service;

import com.guosen.zebra.framework.starter.sso.dto.EacResponse;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

/**
 * SSO服务
 */
public interface SsoService {

    /**
     * 提交表单到sso请求
     * @param returnUrl 回调地址
     * @param response response
     * @return 提交表单页
     */
    String submitFormToSsoPage(String returnUrl, ServerRequest request, ServerResponse.BodyBuilder response);

    /**
     * sso系统验证通过后跳转请求
     * @param response response
     * @return 当前系统验证通过后的重定向地址
     */
    String ssoReturn(ServerRequest request, ServerResponse.BodyBuilder response, EacResponse eacResponse);
}
