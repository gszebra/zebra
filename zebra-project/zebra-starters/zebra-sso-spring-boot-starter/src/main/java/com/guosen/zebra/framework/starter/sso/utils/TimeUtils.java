package com.guosen.zebra.framework.starter.sso.utils;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public final class TimeUtils {
    private TimeUtils() {
    }

    private static final String LOCAL_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final String LOCAL_FORMAT_WITH_MILL = "yyyy-MM-dd HH:mm:ss.SSS";

    private static final String UTC_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private static final String UTC_FORMAT_WITH_SECOND_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private static final int SHANGHAI_TIME_ZONE_DIFF = 8;

    public static String utcToLocalWithMill(String utc) {
        ZonedDateTime zdt = ZonedDateTime.parse(utc);
        LocalDateTime localDateTime = zdt.toLocalDateTime();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LOCAL_FORMAT_WITH_MILL);
        return formatter.format(localDateTime.plusHours(SHANGHAI_TIME_ZONE_DIFF));
    }

    public static String localToUtc(String localTime) {
        DateTimeFormatter LocalFormatter = DateTimeFormatter.ofPattern(LOCAL_FORMAT);
        LocalDateTime localDateTime = LocalDateTime.parse(localTime, LocalFormatter);

        DateTimeFormatter utcFormatter = DateTimeFormatter.ofPattern(UTC_FORMAT_PATTERN);
        return utcFormatter.format(localDateTime.minusHours(SHANGHAI_TIME_ZONE_DIFF));
    }

    public static String timeStamp() {
        DateTimeFormatter utcFormatter = DateTimeFormatter.ofPattern(UTC_FORMAT_PATTERN);
        LocalDateTime localDateTime = LocalDateTime.now();
        return localDateTime.format(utcFormatter);
    }

    public static LocalDateTime addMinuteForDate(int minutes) {
        LocalDateTime dateTime = LocalDateTime.now();
        dateTime = dateTime.plusMinutes(minutes);
        return dateTime;
    }

    public static LocalDateTime strToDateTime(String dateStr) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(LOCAL_FORMAT);
        return LocalDateTime.parse(dateStr, df);
    }

    public static String timeStampOfSeconds(LocalDateTime localDateTime) {
        Double newTime = (double) localDateTime.minusMinutes(1L).toInstant(ZoneOffset.of("+8")).toEpochMilli() / (double) 1000;
        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        return nf.format(newTime);
    }

    public static LocalDateTime utcToDateTime(String utc) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(UTC_FORMAT_WITH_SECOND_PATTERN);
        return LocalDateTime.parse(utc, df).plusHours(SHANGHAI_TIME_ZONE_DIFF);
    }

    public static LocalDateTime utcToLocalDateTime(String dateStr) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(UTC_FORMAT_WITH_SECOND_PATTERN);
        return LocalDateTime.parse(dateStr, df);
    }

    public static String localDateTimeToStrTime(LocalDateTime localDateTime) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(LOCAL_FORMAT);
        return df.format(localDateTime);
    }

    public static String utcTimeToLocalTime(String utc){
        LocalDateTime localDateTime = utcToLocalDateTime(utc).plusHours(SHANGHAI_TIME_ZONE_DIFF);;
        return localDateTimeToStrTime(localDateTime);
    }

    public static String DateToStr(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(LOCAL_FORMAT);
        return format.format(date);
    }

    public static long hoursToMilliseconds(int hours) {
        return hours * 60 * 60 * 1000L;
    }

    public static int hoursToSeconds(int hours) {
        return hours * 60 * 60;
    }

}

