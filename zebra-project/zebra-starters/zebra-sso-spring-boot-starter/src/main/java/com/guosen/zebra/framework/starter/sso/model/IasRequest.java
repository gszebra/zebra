package com.guosen.zebra.framework.starter.sso.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IasRequest {

    /**
     * SSO分配的ID
     */
    private String iasId;

    /**
     * SSO分配的key
     */
    private String iasKey;

    /**
     * 请求时间戳
     */
    private String timeStamp;

    /**
     * SSO登录成功后会将用户信息post到这个url
     */
    private String returnUrl;
}
