package com.guosen.zebra.framework.starter.sso.utils;

public final class Hex
{
    private Hex(){}

    public static byte[] decode(String string) {
        byte[] bytes = new byte[string.length() / 2];
        String buf = string.toLowerCase();

        for (int i = 0; i < buf.length(); i += 2) {
            char left = buf.charAt(i);
            char right = buf.charAt(i + 1);
            int index = i / 2;

            if (left < 'a') {
                bytes[index] = (byte)(left - '0' << '\004');
            }
            else {
                bytes[index] = (byte)(left - 'a' + '\n' << '\004');
            }
            if (right < 'a') {
                bytes[index] = (byte)(bytes[index] + (byte)(right - '0'));
            }
            else {
                bytes[index] = (byte)(bytes[index] + (byte)(right - 'a' + '\n'));
            }
        }

        return bytes;
    }
}
