package com.guosen.zebra.framework.starter.sso.service.impl;

import com.guosen.zebra.framework.starter.sso.constant.SsoConstant;
import com.guosen.zebra.framework.starter.sso.properties.SsoProperties;
import com.guosen.zebra.framework.starter.sso.vo.SsoLoginVO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.function.ServerRequest;

import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SsoSupportServiceImplTest {

    private static final String TEMPLATES_PATH = "defaultSsoTemplates";
    private static final String HOME_PAGE = "/";

    @Mock
    private SsoProperties ssoProperties;

    @Mock
    private ServerRequest request;

    @InjectMocks
    private SsoSupportServiceImpl ssoSupportService;


    @Test
    void testRedirectPage() {
        String returnUrl = "";
        when(ssoProperties.getHomePage()).thenReturn(HOME_PAGE);
        String page = ssoSupportService.redirectPage(returnUrl);
        assertThat(page.startsWith(SsoConstant.REDIRECT_PREFIX), is(true));
    }

    @Test
    void testLoginSuccessPage() {
        String page = ssoSupportService.loginSuccessPage();
        assertThat(page.endsWith(SsoConstant.SUCCESS_TEMPLATE), is(true));
    }

    @Test
    void testErrorPage() {
        String errorMsg = "error message!";
        when(ssoProperties.getTemplatesPath()).thenReturn(TEMPLATES_PATH);
        when(request.attributes()).thenReturn(new HashMap<>());
        String page = ssoSupportService.errorPage(errorMsg, request);
        assertThat(page.endsWith(SsoConstant.ERROR_TEMPLATE), is(true));
    }

    @Test
    void testAutoPostLoginPage() {
        when(ssoProperties.getTemplatesPath()).thenReturn(TEMPLATES_PATH);
        SsoLoginVO ssoLoginVO = SsoLoginVO.builder().build();
        when(request.attributes()).thenReturn(new HashMap<>());
        String page = ssoSupportService.autoPostLoginPage(ssoLoginVO, request);
        assertThat(page.endsWith(SsoConstant.AUTO_POST_LOGIN_TEMPLATE), is(true));
    }
}
