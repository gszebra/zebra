package com.guosen.zebra.framework.starter.sso.service.impl;

import com.guosen.zebra.framework.starter.sso.constant.SsoConstant;
import com.guosen.zebra.framework.starter.sso.dto.EacResponse;
import com.guosen.zebra.framework.starter.sso.properties.SsoProperties;
import com.guosen.zebra.framework.starter.sso.service.SsoCallBackService;
import com.guosen.zebra.framework.starter.sso.service.SsoSupportService;
import com.guosen.zebra.framework.starter.sso.vo.SsoLoginVO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ExtendWith(MockitoExtension.class)
class SsoServiceImplTest {

    private static final String IAS_ID = "11111";
    private static final String IAS_KEY = "22222";
    private static final String BASE_URL = "http://localhost:8080";
    private static final String EAC_ADDRESS = "https://eac.com/login.aspx";
    private static final String HOME_PAGE = "/";

    @Mock
    private SsoProperties ssoProperties;

    @Mock
    private SsoCallBackService ssoCallBackService;

    @Mock
    private ServerRequest request;

    @Mock
    private ServerResponse.BodyBuilder response;

    @Mock
    private SsoSupportService ssoSupportService;

    @InjectMocks
    private SsoServiceImpl ssoService;

    @Test
    void testSubmitFormToSsoPage() {
        String returnUrl = "";

        when(ssoProperties.getIasId()).thenReturn(IAS_ID);
        when(ssoProperties.getIasKey()).thenReturn(IAS_KEY);
        when(ssoProperties.getBaseUrl()).thenReturn(BASE_URL);
        when(ssoProperties.getEacAddress()).thenReturn(EAC_ADDRESS);
        when(ssoSupportService.autoPostLoginPage(any(SsoLoginVO.class), any(ServerRequest.class)))
                .thenReturn(SsoConstant.AUTO_POST_LOGIN_TEMPLATE);
        String page = ssoService.submitFormToSsoPage(returnUrl, request, response);
        assertThat(page.startsWith(SsoConstant.AUTO_POST_LOGIN_TEMPLATE), is(true));
        verify(ssoSupportService).autoPostLoginPage(any(SsoLoginVO.class), any(ServerRequest.class));
    }

    @Test
    void testSsoReturn() {
        EacResponse eacResponse = new EacResponse();

        when(ssoCallBackService.handleLoginSuccess(any(ServerRequest.class), any(ServerResponse.BodyBuilder.class), any(EacResponse.class)))
                .thenReturn(null);
        when(ssoSupportService.redirectPage(anyString())).thenReturn(SsoConstant.AUTO_POST_LOGIN_TEMPLATE);
        when(request.cookies()).thenReturn(new LinkedMultiValueMap<>());
        String page = ssoService.ssoReturn(request, response, eacResponse);
        assertThat(page, is(SsoConstant.AUTO_POST_LOGIN_TEMPLATE));
        verify(ssoCallBackService).handleLoginSuccess(any(ServerRequest.class), any(ServerResponse.BodyBuilder.class), any(EacResponse.class));
        verify(ssoSupportService).redirectPage(anyString());
    }
}
