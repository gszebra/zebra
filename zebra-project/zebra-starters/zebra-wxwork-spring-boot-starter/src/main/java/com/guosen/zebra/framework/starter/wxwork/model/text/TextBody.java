package com.guosen.zebra.framework.starter.wxwork.model.text;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文本消息请求内容
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TextBody {

    /**
     * 消息内容
     */
    private String content;
}
