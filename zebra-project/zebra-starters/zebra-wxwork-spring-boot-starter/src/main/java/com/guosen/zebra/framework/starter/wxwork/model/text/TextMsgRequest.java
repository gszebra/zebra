package com.guosen.zebra.framework.starter.wxwork.model.text;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.guosen.zebra.framework.starter.wxwork.properties.WXWorkProperties;
import com.guosen.zebra.framework.starter.wxwork.model.MsgReqCommon;
import lombok.Builder;
import lombok.Data;

/**
 * 文本消息请求参数
 */
@Data
@Builder
public class TextMsgRequest {

    /**
     * 请求代理配置，一般无需设置
     */
    private WXWorkProperties.Agent agent;

    /**
     * 公共请求部分，按需设置
     */
    private MsgReqCommon common;

    /**
     * 文本消息体
     */
    private TextBody text;

    /**
     * 表示是否是保密消息，0表示可对外分享，1表示不能分享且内容显示水印，默认为0（不传默认为0）
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer safe;

    /**
     * 表示是否开启id转译，0表示否，1表示是，默认0。仅第三方应用需要用到，企业自建应用可以忽略。（不传默认为0）
     */
    @JsonProperty("enable_id_trans")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer enableIdTrans;
}
