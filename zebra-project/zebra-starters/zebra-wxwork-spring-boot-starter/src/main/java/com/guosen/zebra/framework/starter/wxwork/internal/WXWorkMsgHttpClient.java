package com.guosen.zebra.framework.starter.wxwork.internal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.guosen.zebra.framework.basic.http.OKHttpClientUtil;
import com.guosen.zebra.framework.starter.wxwork.exception.WXWorkException;
import com.guosen.zebra.framework.starter.wxwork.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * 企微消息http客户端
 */
@Slf4j
public class WXWorkMsgHttpClient {

    private WXWorkMsgHttpClient() {}

    /**
     * JSON类型
     */
    public static final MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");

    /**
     * 二进制流类型
     */
    public static final MediaType APPLICATION_OCT_STREAM = MediaType.parse("application/octet-stream");

    /**
     * http协议客户端
     */
    private static final OkHttpClient HTTP_CLIENT = OKHttpClientUtil.getHttpClient();

    /**
     * https客户端，忽略证书
     */
    private static final OkHttpClient UNSAFE_HTTPS_CLIENT = OKHttpClientUtil.getUnsafeHttpsClient();


    public static JsonNode post(String url, Map<String, String> headers, Object parameters)
            throws WXWorkException {
        String jsonParam = object2String(parameters);
        Request request = buildRequest(url, headers, jsonParam);
        return doInvoke(request, url);
    }

    private static Request buildRequest(String url, Map<String, String> headers, String jsonParam) {
        try {
            RequestBody body = RequestBody.create(JSON_TYPE, jsonParam);
            Request.Builder builder = new Request.Builder().url(url).post(body);
            setHeaders(headers, builder);
            return builder.build();
        } catch (Exception e) {
            log.error("Failed to build request, url: {}, error message : {}", url, e.getMessage(), e);
            throw new WXWorkException("请求创建异常！", e);
        }
    }

    private static String object2String(Object object) {
        try {
            return JsonUtil.getObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new WXWorkException(e);
        }
    }

    private static void setHeaders(Map<String, String> headers, Request.Builder builder) {
        for (Map.Entry<String, String> header : headers.entrySet()) {
            builder.header(header.getKey(), header.getValue());
        }
    }

    private static JsonNode doInvoke(Request request, String url) throws WXWorkException {
        JsonNode result = null;
        try (Response response = getHttpClient(url).newCall(request).execute();
             ResponseBody responseBody = response.body()) {
            if (responseBody != null) {
                String respString = responseBody.string();
                if (StringUtils.isNotBlank(respString)) {
                    result = JsonUtil.getObjectMapper().readTree(respString);
                }
            }
        } catch (Exception e) {
            log.error("Failed to invoke {}, error message : {}", url, e.getMessage(), e);
            throw new WXWorkException("500", e.getMessage());
        }
        return result;
    }

    private static OkHttpClient getHttpClient(String url) {
        boolean isHttps = StringUtils.startsWithIgnoreCase(url, "https");
        return isHttps ? UNSAFE_HTTPS_CLIENT : HTTP_CLIENT;
    }
}
