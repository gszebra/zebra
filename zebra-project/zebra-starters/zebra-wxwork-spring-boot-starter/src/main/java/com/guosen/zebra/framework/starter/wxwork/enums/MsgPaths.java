package com.guosen.zebra.framework.starter.wxwork.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 应用消息路径
 */
@Getter
@AllArgsConstructor
public enum MsgPaths {

    TEXT("/textMessage", "发送文本消息接口"),
    MARK_DOWN("/markdownMessage", "发送markdown消息接口");

    private final String path;
    private final String description;
}
