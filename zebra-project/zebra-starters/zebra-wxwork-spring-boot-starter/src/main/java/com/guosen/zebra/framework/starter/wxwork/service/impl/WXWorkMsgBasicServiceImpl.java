package com.guosen.zebra.framework.starter.wxwork.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.guosen.zebra.framework.starter.wxwork.properties.WXWorkProperties;
import com.guosen.zebra.framework.starter.wxwork.enums.MsgPaths;
import com.guosen.zebra.framework.starter.wxwork.exception.WXWorkException;
import com.guosen.zebra.framework.starter.wxwork.internal.WXWorkMsgHttpClient;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkMsgBasicService;
import com.guosen.zebra.framework.starter.wxwork.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * 企微消息基础服务接口实现
 */
@Slf4j
public class WXWorkMsgBasicServiceImpl implements WXWorkMsgBasicService {


    private final WXWorkProperties wxWorkProperties;

    public WXWorkMsgBasicServiceImpl(WXWorkProperties wxWorkProperties) {
        this.wxWorkProperties = wxWorkProperties;
    }

    @Override
    public TextMsgResponse sendTextMessage(TextMsgRequest request) throws WXWorkException {
        request.setAgent(wxWorkProperties.getAgent());
        String textMessageUrl = getTextMessageUrl();
        JsonNode result;
        log.info("[WXWork] Text message is sending ..., request:{}", request);
        try {
            result = WXWorkMsgHttpClient.post(textMessageUrl, new HashMap<>(), request);
        } catch (WXWorkException e) {
            log.error("[WXWork] Text message post failed, request:{}, error message:{}", request, e.getMessage(), e);
            throw new WXWorkException("Text消息发送失败！", e);
        }
        try {
            return JsonUtil.getObjectMapper().treeToValue(result, TextMsgResponse.class);
        } catch (JsonProcessingException e) {
            log.error("[WXWork] Text message response parsed failed, response:{}, error message:{}", result.toString(), e.getMessage(), e);
            throw new WXWorkException("响应数据处理异常" + result, e);
        }
    }


    @Override
    public MarkdownMsgResponse sendMarkdownMessage(MarkdownMsgRequest request) throws WXWorkException {
        request.setAgent(wxWorkProperties.getAgent());
        String textMessageUrl = getMarkdownMessageUrl();
        JsonNode result;
        log.info("[WXWork] Markdown Message is sending ..., request:{}", request);
        try {
            result = WXWorkMsgHttpClient.post(textMessageUrl, new HashMap<>(), request);
        } catch (WXWorkException e) {
            log.error("[WXWork] Markdown message post failed, request:{}, error message:{}", request, e.getMessage(), e);
            throw new WXWorkException("Markdown消息发送失败！", e);
        }
        try {
            return JsonUtil.getObjectMapper().treeToValue(result, MarkdownMsgResponse.class);
        } catch (JsonProcessingException e) {
            log.error("[WXWork] Markdown message response parsed failed, response:{}, error message:{}", result.toString(), e.getMessage(), e);
            throw new WXWorkException("响应数据处理异常！" + result, e);
        }
    }

    private String getTextMessageUrl() { return wxWorkProperties.getMsgBaseUrl() + MsgPaths.TEXT.getPath(); }

    private String getMarkdownMessageUrl() { return wxWorkProperties.getMsgBaseUrl() + MsgPaths.MARK_DOWN.getPath(); }
}
