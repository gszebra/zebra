package com.guosen.zebra.framework.starter.wxwork.properties;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 企微中台配置
 */
@Data
@ConfigurationProperties(prefix = "zebra.wxwork")
public class WXWorkProperties {

    /**
     * 应用消息请求基路径
     */
    private String msgBaseUrl;

    /**
     * 请求代理配置
     */
    private Agent agent;

    @Data
    public static class Agent {

        /**
         * 应用id
         */
        @JsonProperty("agentid")
        private String agentId;

        /**
         * 调用者系统id
         */
        @JsonProperty("appid")
        private String appId;

        /**
         * 调用者密码验证字段
         */
        @JsonProperty("appsecret")
        private String appSecret;

        /**
         * 企业id
         */
        @JsonProperty("corpid")
        private String corpId;
    }
}
