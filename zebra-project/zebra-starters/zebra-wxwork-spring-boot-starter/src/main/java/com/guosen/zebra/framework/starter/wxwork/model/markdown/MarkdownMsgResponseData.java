package com.guosen.zebra.framework.starter.wxwork.model.markdown;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * markdown消息响应的业务数据
 */
@Data
public class MarkdownMsgResponseData {

    /**
     * 无效的标签
     */
    @JsonProperty("invalidtag")
    private String invalidTags;

    /**
     * 无效的部门
     */
    @JsonProperty("invalidparty")
    private String invalidParties;

    /**
     * 无效的成员
     */
    @JsonProperty("invaliduser")
    private String invalidUsers;

    @JsonProperty("response_code")
    private String responseCode;

    @JsonProperty("unlicenseduser")
    private String unLicensedUsers;

    @JsonProperty("msgid")
    private String msgId;
}
