package com.guosen.zebra.framework.starter.wxwork.exception;

import org.apache.commons.lang3.StringUtils;

/**
 * 企微中台接口调用异常
 */
public class WXWorkException extends RuntimeException {

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    public WXWorkException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public WXWorkException(String message, Throwable cause) {
        super(message, cause);
    }

    public WXWorkException(Exception e) {
        super(e);
    }

    @Override
    public String getMessage() {
        return StringUtils.isNotEmpty(this.message) ? this.message : super.getMessage();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
