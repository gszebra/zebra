package com.guosen.zebra.framework.starter.wxwork.service;

import com.guosen.zebra.framework.starter.wxwork.exception.WXWorkException;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgResponse;

/**
 * 企业微信消息基础服务
 */
public interface WXWorkMsgBasicService {

    /**
     * 发送普通文本消息
     * @param request 文本消息请求参数
     */
    TextMsgResponse sendTextMessage(TextMsgRequest request) throws WXWorkException;

    /**
     * 发送markdown消息
     * @return markdown消息请求参数
     */
    MarkdownMsgResponse sendMarkdownMessage(MarkdownMsgRequest request) throws WXWorkException;
}
