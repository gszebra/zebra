package com.guosen.zebra.framework.starter.wxwork.model.markdown;

import com.guosen.zebra.framework.starter.wxwork.properties.WXWorkProperties;
import com.guosen.zebra.framework.starter.wxwork.model.MsgReqCommon;
import lombok.Builder;
import lombok.Data;

/**
 * markdown消息请求参数
 */
@Data
@Builder
public class MarkdownMsgRequest {

    /**
     * 请求代理配置，一般无需设置
     */
    private WXWorkProperties.Agent agent;

    /**
     * 公共请求部分，按需设置
     */
    private MsgReqCommon common;

    /**
     * markdown消息内容
     */
    private MarkdownBody markdown;
}
