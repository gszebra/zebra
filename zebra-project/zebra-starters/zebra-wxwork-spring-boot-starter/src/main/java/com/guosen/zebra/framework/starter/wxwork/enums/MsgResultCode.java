package com.guosen.zebra.framework.starter.wxwork.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MsgResultCode {

    SUCCESS(0, "成功");

    private final int code;
    private final String description;
}
