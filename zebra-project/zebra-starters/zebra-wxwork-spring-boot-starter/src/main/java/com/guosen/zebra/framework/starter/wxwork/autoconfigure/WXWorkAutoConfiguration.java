package com.guosen.zebra.framework.starter.wxwork.autoconfigure;

import com.guosen.zebra.framework.starter.wxwork.properties.WXWorkProperties;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkMarkdownMsgService;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkMsgBasicService;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkTextMsgService;
import com.guosen.zebra.framework.starter.wxwork.service.impl.WXWorkMarkdownMsgServiceImpl;
import com.guosen.zebra.framework.starter.wxwork.service.impl.WXWorkMsgBasicServiceImpl;
import com.guosen.zebra.framework.starter.wxwork.service.impl.WXWorkTextMsgServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 企业微信自动配置类
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(WXWorkProperties.class)
public class WXWorkAutoConfiguration {

    @Bean
    public WXWorkMsgBasicService wxWorkMsgBasicService(WXWorkProperties wxWorkProperties) {
        return new WXWorkMsgBasicServiceImpl(wxWorkProperties);
    }

    @Bean
    public WXWorkTextMsgService wxWorkTextMsgService(WXWorkMsgBasicService basicService) {
        return new WXWorkTextMsgServiceImpl(basicService);
    }

    @Bean
    public WXWorkMarkdownMsgService wxWorkMarkdownMsgService(WXWorkMsgBasicService basicService) {
        return new WXWorkMarkdownMsgServiceImpl(basicService);
    }
}
