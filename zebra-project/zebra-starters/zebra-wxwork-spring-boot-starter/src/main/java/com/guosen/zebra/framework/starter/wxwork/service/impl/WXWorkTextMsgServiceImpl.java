package com.guosen.zebra.framework.starter.wxwork.service.impl;

import com.guosen.zebra.framework.starter.wxwork.enums.MsgTypes;
import com.guosen.zebra.framework.starter.wxwork.exception.WXWorkException;
import com.guosen.zebra.framework.starter.wxwork.model.MsgReqCommon;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextBody;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkMsgBasicService;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkTextMsgService;

/**
 * 企微文本消息服务实现
 */
public class WXWorkTextMsgServiceImpl implements WXWorkTextMsgService {

    private final WXWorkMsgBasicService basicService;

    public WXWorkTextMsgServiceImpl(WXWorkMsgBasicService basicService) {
        this.basicService = basicService;
    }

    @Override
    public TextMsgResponse sendMessageToUsers(String content, String toUsers) throws WXWorkException {
        TextBody text = new TextBody(content);
        MsgReqCommon common = MsgReqCommon
                .builder().msgType(MsgTypes.TEXT.getType()).toUsers(toUsers).build();
        TextMsgRequest textMsgRequest = TextMsgRequest
                .builder().text(text).common(common).build();
        return basicService.sendTextMessage(textMsgRequest);
    }
}
