package com.guosen.zebra.framework.starter.wxwork.model.text;

import com.guosen.zebra.framework.starter.wxwork.model.MsgResult;
import lombok.Data;

/**
 * 文本消息响应参数
 */
@Data
public class TextMsgResponse {

    /**
     * 返回结果
     */
    private MsgResult result;

    /**
     * 文本消息响应的业务数据
     */
    private TextMsgResponseData data;
}
