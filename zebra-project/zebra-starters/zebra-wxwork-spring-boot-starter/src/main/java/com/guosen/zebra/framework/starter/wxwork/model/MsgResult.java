package com.guosen.zebra.framework.starter.wxwork.model;

import lombok.Data;

/**
 * 消息发送结果
 */
@Data
public class MsgResult {

    /**
     * 返回码， 0表示成功，其它表示失败
     */
    private Integer code;

    /**
     * msg（请求结果消息提示），如ok表示成功
     */
    private String msg;
}
