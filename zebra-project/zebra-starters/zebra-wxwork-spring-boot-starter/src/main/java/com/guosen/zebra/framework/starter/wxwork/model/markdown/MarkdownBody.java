package com.guosen.zebra.framework.starter.wxwork.model.markdown;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * markdown消息请求内容
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarkdownBody {

    /**
     * 消息内容
     */
    private String content;
}
