package com.guosen.zebra.framework.starter.wxwork.service;

import com.guosen.zebra.framework.starter.wxwork.exception.WXWorkException;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgResponse;

/**
 * 企微markdown消息服务接口
 */
public interface WXWorkMarkdownMsgService {

    /**
     * 发送markdown消息
     * @param content 消息内容
     * @param toUsers 指定接收消息的成员，成员ID列表（多个用竖线分隔，最多支持1000个）。指定为”@all”，则向该企业应用的全部成员发送；示例：06103|wb122
     */
    MarkdownMsgResponse sendMessageToUsers(String content, String toUsers) throws WXWorkException;
}
