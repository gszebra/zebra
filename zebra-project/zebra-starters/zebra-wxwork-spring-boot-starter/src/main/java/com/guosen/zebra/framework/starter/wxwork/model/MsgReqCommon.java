package com.guosen.zebra.framework.starter.wxwork.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * 消息请求参数-公共部分
 */
@Data
@Builder
public class MsgReqCommon {

    /**
     * 指定接收消息的成员，成员ID列表（多个用竖线分隔，最多支持1000个）。
     * 指定为”@all”，则向该企业应用的全部成员发送
     */
    @JsonProperty("touser")
    private String toUsers;

    /**
     *  指定接收消息的部门，部门ID列表，多个接收者用竖线分隔，最多支持100个。
     *  当touser为”@all”时忽略本参数
     */
    @JsonProperty("toparty")
    private String toParties;

    /**
     * 指定接收消息的标签，标签ID列表，多个接收者用竖线分隔，最多支持100个。
     * 当touser为”@all”时忽略本参数
     */
    @JsonProperty("totag")
    private String toTags;

    /**
     * 消息类型
     */
    @JsonProperty("msgtype")
    private String msgType;

    /**
     * 表示是否开启重复消息检查，0表示否，1表示是，默认0（不传参默认为0）
     */
    @JsonProperty("enable_duplicate_check")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer enableDuplicateCheck;

    /**
     * 表示是否重复消息检查的时间间隔，默认1800s，最大不超过4小时 （不传参默认为0）
     */
    @JsonProperty("duplicate_check_interval")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer duplicateCheckInterval;
}
