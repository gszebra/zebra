package com.guosen.zebra.framework.starter.wxwork.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 应用消息类型
 */
@Getter
@AllArgsConstructor
public enum MsgTypes {
    TEXT("text", "文本消息"),
    MARK_DOWN("markdown", "markdown消息");

    private final String type;
    private final String description;
}
