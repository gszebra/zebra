package com.guosen.zebra.framework.starter.wxwork.model.text;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 文本消息响应内容
 */
@Data
public class TextMsgResponseData {

    /**
     * 无效的标签
     */
    @JsonProperty("invalidtag")
    private String invalidTags;

    /**
     * 无效的部门
     */
    @JsonProperty("invalidparty")
    private String invalidParties;

    /**
     * 无效的成员
     */
    @JsonProperty("invaliduser")
    private String invalidUsers;

    @JsonProperty("response_code")
    private String responseCode;

    @JsonProperty("unlicenseduser")
    private String unLicensedUsers;

    @JsonProperty("msgid")
    private String msgId;
}
