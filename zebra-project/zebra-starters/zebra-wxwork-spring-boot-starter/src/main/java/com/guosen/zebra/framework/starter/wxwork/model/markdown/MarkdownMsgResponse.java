package com.guosen.zebra.framework.starter.wxwork.model.markdown;

import com.guosen.zebra.framework.starter.wxwork.model.MsgResult;
import lombok.Data;

/**
 * Markdown消息响应参数
 */
@Data
public class MarkdownMsgResponse {

    /**
     * 返回结果
     */
    private MsgResult result;

    /**
     * markdown消息响应的业务数据
     */
    private MarkdownMsgResponseData data;
}
