package com.guosen.zebra.framework.starter.wxwork.service.impl;

import com.guosen.zebra.framework.starter.wxwork.enums.MsgTypes;
import com.guosen.zebra.framework.starter.wxwork.exception.WXWorkException;
import com.guosen.zebra.framework.starter.wxwork.model.MsgReqCommon;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownBody;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkMarkdownMsgService;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkMsgBasicService;

/**
 * 企微markdown消息服务实现
 */
public class WXWorkMarkdownMsgServiceImpl implements WXWorkMarkdownMsgService {

    private final WXWorkMsgBasicService basicService;

    public WXWorkMarkdownMsgServiceImpl(WXWorkMsgBasicService basicService) {
        this.basicService = basicService;
    }

    @Override
    public MarkdownMsgResponse sendMessageToUsers(String content, String toUsers) throws WXWorkException {
        MarkdownBody markdown = new MarkdownBody(content);
        MsgReqCommon common = MsgReqCommon
                .builder().msgType(MsgTypes.MARK_DOWN.getType()).toUsers(toUsers).build();
        MarkdownMsgRequest markdownMsgRequest = MarkdownMsgRequest
                .builder().markdown(markdown).common(common).build();
        return basicService.sendMarkdownMessage(markdownMsgRequest);
    }
}
