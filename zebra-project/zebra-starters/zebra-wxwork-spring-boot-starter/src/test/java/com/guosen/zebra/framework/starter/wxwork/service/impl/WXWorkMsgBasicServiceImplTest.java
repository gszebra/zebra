package com.guosen.zebra.framework.starter.wxwork.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.guosen.zebra.framework.starter.wxwork.enums.MsgResultCode;
import com.guosen.zebra.framework.starter.wxwork.enums.MsgTypes;
import com.guosen.zebra.framework.starter.wxwork.exception.WXWorkException;
import com.guosen.zebra.framework.starter.wxwork.internal.WXWorkMsgHttpClient;
import com.guosen.zebra.framework.starter.wxwork.model.MsgReqCommon;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownBody;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextBody;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.properties.WXWorkProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WXWorkMsgBasicServiceImplTest {

    private static final String BASE_URL = "http://127.0.0.1/xxxxx/iiii/";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Mock
    private WXWorkProperties wxWorkProperties;

    @InjectMocks
    private WXWorkMsgBasicServiceImpl wxWorkMsgBasicService;


    @Test
    void testSendTextMessage() {
        String content = "text_content";
        String toUsers = "10086|10010";
        TextBody text = new TextBody(content);
        MsgReqCommon common = MsgReqCommon
                .builder().msgType(MsgTypes.TEXT.getType()).toUsers(toUsers).build();
        TextMsgRequest textMsgRequest = TextMsgRequest
                .builder().text(text).common(common).build();

        when(wxWorkProperties.getMsgBaseUrl()).thenReturn(BASE_URL);
        try (MockedStatic<WXWorkMsgHttpClient> mockedHttpClient = org.mockito.Mockito.mockStatic(WXWorkMsgHttpClient.class)) {
            mockedHttpClient.when(() -> WXWorkMsgHttpClient.post(anyString(), any(), any())).thenReturn(getMockJsonResult());
            TextMsgResponse textMsgResponse = wxWorkMsgBasicService.sendTextMessage(textMsgRequest);
            assertThat(textMsgResponse.getResult().getCode(), is(MsgResultCode.SUCCESS.getCode()));
        }
    }

    @Test
    void testSendTextMessageWithException() {
        String content = "text_content";
        String toUsers = "10086|10010";
        TextBody text = new TextBody(content);
        MsgReqCommon common = MsgReqCommon
                .builder().msgType(MsgTypes.TEXT.getType()).toUsers(toUsers).build();
        TextMsgRequest textMsgRequest = TextMsgRequest
                .builder().text(text).common(common).build();
        when(wxWorkProperties.getMsgBaseUrl()).thenReturn(BASE_URL);
        try (MockedStatic<WXWorkMsgHttpClient> mockedHttpClient = org.mockito.Mockito.mockStatic(WXWorkMsgHttpClient.class)) {
            mockedHttpClient.when(() -> WXWorkMsgHttpClient.post(anyString(), any(), any())).thenThrow(new WXWorkException(new IOException("wxwork exception")));
            assertThrows(WXWorkException.class, () -> wxWorkMsgBasicService.sendTextMessage(textMsgRequest));
        }
    }

    @Test
    void testSendMarkdownMessage() {
        String content = "markdown_content";
        String toUsers = "10086|10010";
        MarkdownBody markdown = new MarkdownBody(content);
        MsgReqCommon common = MsgReqCommon
                .builder().msgType(MsgTypes.MARK_DOWN.getType()).toUsers(toUsers).build();
        MarkdownMsgRequest markdownMsgRequest = MarkdownMsgRequest
                .builder().markdown(markdown).common(common).build();
        when(wxWorkProperties.getMsgBaseUrl()).thenReturn(BASE_URL);
        try (MockedStatic<WXWorkMsgHttpClient> mockedHttpClient = org.mockito.Mockito.mockStatic(WXWorkMsgHttpClient.class)) {
            mockedHttpClient.when(() -> WXWorkMsgHttpClient.post(anyString(), any(), any())).thenReturn(getMockJsonResult());
            MarkdownMsgResponse markdownMsgResponse = wxWorkMsgBasicService.sendMarkdownMessage(markdownMsgRequest);
            assertThat(markdownMsgResponse.getResult().getCode(), is(MsgResultCode.SUCCESS.getCode()));
        }
    }

    @Test
    void testSendMarkdownMessageWithException() {
        String content = "markdown_content";
        String toUsers = "10086|10010";
        MarkdownBody markdown = new MarkdownBody(content);
        MsgReqCommon common = MsgReqCommon
                .builder().msgType(MsgTypes.MARK_DOWN.getType()).toUsers(toUsers).build();
        MarkdownMsgRequest markdownMsgRequest = MarkdownMsgRequest
                .builder().markdown(markdown).common(common).build();
        when(wxWorkProperties.getMsgBaseUrl()).thenReturn(BASE_URL);
        try (MockedStatic<WXWorkMsgHttpClient> mockedHttpClient = org.mockito.Mockito.mockStatic(WXWorkMsgHttpClient.class)) {
            mockedHttpClient.when(() -> WXWorkMsgHttpClient.post(anyString(), any(), any())).thenThrow(new WXWorkException(new IOException("wxwork exception")));
            assertThrows(WXWorkException.class, () -> wxWorkMsgBasicService.sendMarkdownMessage(markdownMsgRequest));
        }
    }

    private static JsonNode getMockJsonResult() {
        String jsonString = "{\"result\":{\"code\":0,\"msg\":\"suceess\"}}";
        JsonNode jsonNode = null;
        try {
            jsonNode = OBJECT_MAPPER.readTree(jsonString);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return jsonNode;
    }
}
