package com.guosen.zebra.framework.starter.wxwork.service.impl;

import com.guosen.zebra.framework.starter.wxwork.enums.MsgResultCode;
import com.guosen.zebra.framework.starter.wxwork.model.MsgResult;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.text.TextMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkMsgBasicService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * 文本消息单测
 */
@ExtendWith(MockitoExtension.class)
class WXWorkTextMsgServiceImplTest {

    @Mock
    private WXWorkMsgBasicService basicService;

    @InjectMocks
    private WXWorkTextMsgServiceImpl wxWorkTextMsgService;


    @Test
    void testSendMessageToUsers() {
        String content = "text_content";
        String toUsers = "10086|10010";

        when(basicService.sendTextMessage(Mockito.any(TextMsgRequest.class)))
                .thenReturn(getMockResult());
        TextMsgResponse textMsgResponse = wxWorkTextMsgService.sendMessageToUsers(content, toUsers);
        assertThat(textMsgResponse.getResult().getCode(), is(MsgResultCode.SUCCESS.getCode()));
    }

    private static TextMsgResponse getMockResult() {
        TextMsgResponse textMsgResponse = new TextMsgResponse();
        textMsgResponse.setResult(new MsgResult().setCode(MsgResultCode.SUCCESS.getCode()));
        return textMsgResponse;
    }
}
