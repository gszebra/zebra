package com.guosen.zebra.framework.starter.wxwork.service.impl;

import com.guosen.zebra.framework.starter.wxwork.enums.MsgResultCode;
import com.guosen.zebra.framework.starter.wxwork.model.MsgResult;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgRequest;
import com.guosen.zebra.framework.starter.wxwork.model.markdown.MarkdownMsgResponse;
import com.guosen.zebra.framework.starter.wxwork.service.WXWorkMsgBasicService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ExtendWith(MockitoExtension.class)
class WXWorkMarkdownMsgServiceImplTest {

    @Mock
    private WXWorkMsgBasicService basicService;

    @InjectMocks
    private WXWorkMarkdownMsgServiceImpl wxWorkMarkdownMsgService;

    @Test
    void testSendMarkdownMessage() {
        String content = "markdown_content";
        String toUsers = "10086|10010";

        when(basicService.sendMarkdownMessage(any(MarkdownMsgRequest.class))).thenReturn(getMockResult());
        MarkdownMsgResponse markdownMsgResponse = wxWorkMarkdownMsgService.sendMessageToUsers(content, toUsers);
        assertThat(markdownMsgResponse.getResult().getCode(), is(MsgResultCode.SUCCESS.getCode()));
    }

    private static MarkdownMsgResponse getMockResult() {
        MarkdownMsgResponse markdownMsgResponse = new MarkdownMsgResponse();
        markdownMsgResponse.setResult(new MsgResult().setCode(MsgResultCode.SUCCESS.getCode()));
        return markdownMsgResponse;
    }
}
