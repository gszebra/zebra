package com.guosen.zebra.framework.starter.startup.autoconfigure;

import com.guosen.zebra.framework.starter.startup.condition.ConditionalOnNonEmptyProperty;
import com.guosen.zebra.framework.starter.startup.filter.ActuatorFilter;
import org.springframework.boot.actuate.autoconfigure.web.ManagementContextConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ManagementContextConfiguration
@ConditionalOnNonEmptyProperty(value = "management.zebra.token")
public class StartupWebMvcConfigurer implements WebMvcConfigurer {

    @Bean
    public ActuatorFilter actuatorFilter() { return new ActuatorFilter(); }

    @Bean
    public FilterRegistrationBean<ActuatorFilter> interfaceLogFilterRegistration() {
        FilterRegistrationBean<ActuatorFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.addUrlPatterns("/actuator/*");
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        filterRegistrationBean.setFilter(actuatorFilter());
        return filterRegistrationBean;
    }
}
