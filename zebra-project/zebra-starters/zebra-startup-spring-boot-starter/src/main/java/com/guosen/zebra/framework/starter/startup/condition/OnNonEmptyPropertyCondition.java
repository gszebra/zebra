package com.guosen.zebra.framework.starter.startup.condition;


import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.StringUtils;

public class OnNonEmptyPropertyCondition implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String key = (String) metadata.getAnnotationAttributes(ConditionalOnNonEmptyProperty.class.getName()).get("value");
        String value = context.getEnvironment().getProperty(key);
        return StringUtils.hasLength(value);
    }
}
