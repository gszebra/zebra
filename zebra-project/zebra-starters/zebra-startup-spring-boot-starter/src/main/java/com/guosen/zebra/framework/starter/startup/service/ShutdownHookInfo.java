package com.guosen.zebra.framework.starter.startup.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;

@Slf4j
public class ShutdownHookInfo implements CommandLineRunner {
    @Override
    public void run(String... args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> log.warn("Receive shutdown request, just print this message")));
    }
}
