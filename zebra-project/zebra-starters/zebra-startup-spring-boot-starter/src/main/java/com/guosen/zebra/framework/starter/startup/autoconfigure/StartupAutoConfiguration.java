package com.guosen.zebra.framework.starter.startup.autoconfigure;

import com.guosen.zebra.framework.starter.startup.service.ShutdownHookInfo;
import com.guosen.zebra.framework.starter.startup.service.ZebraInfoContributor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StartupAutoConfiguration {

    @Bean
    public ShutdownHookInfo shutdownHookInfo() {
        return new ShutdownHookInfo();
    }

    @Bean
    public ZebraInfoContributor zebraInfoContributor() {
        return new ZebraInfoContributor();
    }
}
