package com.guosen.zebra.framework.starter.config.bootstrap;

import com.guosen.zebra.framework.starter.config.properties.ZebraConfigProperties;
import com.guosen.zebra.framework.starter.config.service.impl.ZebraPropertySourceLocator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(ZebraConfigProperties.class)
@ConditionalOnProperty(name = "zebra.config.enabled", matchIfMissing = true)
public class ZebraConfigBootstrapConfiguration {

    @Bean
    public ZebraPropertySourceLocator zebraPropertySourceLocator(ZebraConfigProperties zebraConfigProperties) {
        return new ZebraPropertySourceLocator(zebraConfigProperties);
    }
}
