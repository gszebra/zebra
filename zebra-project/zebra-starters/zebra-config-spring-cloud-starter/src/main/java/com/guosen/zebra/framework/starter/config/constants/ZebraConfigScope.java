package com.guosen.zebra.framework.starter.config.constants;

/**
 * zebra配置范围
 */
public final class ZebraConfigScope {

    private ZebraConfigScope(){}

    public static final String SCOPE_GLOBAL = "global";

    public static final String SCOPE_IDC = "idc";

    public static final String SCOPE_SET = "set";

    public static final String SCOPE_NODE = "node";
}
