package com.guosen.zebra.framework.starter.config.constants;

/**
 * zebra常量类
 */
public class ZebraConstants {
    private ZebraConstants() {}

    public static final String ZEBRA_PROPERTIES_NAME = "localCache.properties";

    public static final String GET_ZEBRA_CONFIG_PATH = "/zebra-conf/getConfNew";
}
