package com.guosen.zebra.framework.starter.config.exception;

import org.apache.commons.lang3.StringUtils;

/**
 * zebra配置中心接口调用异常
 */
public class ZebraConfigException extends RuntimeException {

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    public ZebraConfigException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public ZebraConfigException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZebraConfigException(String message) {
        super();
        this.message = message;
    }

    public ZebraConfigException(Exception e) {
        super(e);
    }

    @Override
    public String getMessage() {
        return StringUtils.isNotEmpty(this.message) ? this.message : super.getMessage();
    }
}
