package com.guosen.zebra.framework.starter.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * zebra配置类
 */
@ConfigurationProperties("zebra.config")
@Data
public class ZebraConfigProperties {

    /**
     * 配置名
     */
    private String name;

    /**
     * zebra配置中心地址
     */
    private String serverAddr;

    /**
     * 是否启用
     */
    private boolean enabled;
}
