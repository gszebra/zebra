package com.guosen.zebra.framework.starter.config.constants;

/**
 * zebra配置类型
 */
public class ZebraConfigType {

    private ZebraConfigType() {}

    /**
     * 资源类配置
     */
    public static final String RES_CONF = "0";

    /**
     * 业务类配置
     */
    public static final String BUSS_CONF = "1";
}
