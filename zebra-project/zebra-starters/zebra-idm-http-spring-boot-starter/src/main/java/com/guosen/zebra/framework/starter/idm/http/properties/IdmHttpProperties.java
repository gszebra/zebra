package com.guosen.zebra.framework.starter.idm.http.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "zebra.idm.http")
public class IdmHttpProperties {

    /**
     * 应用id
     */
    private String appId;

    /**
     * 应用密钥
     */
    private String appSecret;

    /**
     * url
     */
    private String url;

}
