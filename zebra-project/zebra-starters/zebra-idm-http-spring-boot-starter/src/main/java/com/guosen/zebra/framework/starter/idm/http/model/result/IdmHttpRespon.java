package com.guosen.zebra.framework.starter.idm.http.model.result;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @className: IdmHttpRespon
 * @description: Idm http接口的返回结果
 **/
public class IdmHttpRespon {
    /**
     *返回码
     * “0”代表成功,其余均为失败
     */
    @JsonProperty("code")
    private String code;

    /**
     *返回信息
     * “SUCCESS”代表成功，其余均为失败
     */
    @JsonProperty("msg")
    private String msg;

    /**
     * 返回结果值
     *
     * 组织结果集合:
     * 成功返回一般含有：
     * page(页数)
     * size(每页条数)
     * List[]中的同步字段信息是动态的，在IAM的应用配置/字段同步配置中进行配置
     */
    @JsonProperty("data")
    private Object data;

    /**
     * 时间戳
     */
    @JsonProperty("timestamp")
    private Long timestamp;
}
