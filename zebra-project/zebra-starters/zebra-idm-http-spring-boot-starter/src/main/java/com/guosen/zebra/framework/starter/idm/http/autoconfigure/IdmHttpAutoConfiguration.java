package com.guosen.zebra.framework.starter.idm.http.autoconfigure;


import com.guosen.zebra.framework.starter.idm.http.properties.IdmHttpProperties;
import com.guosen.zebra.framework.starter.idm.http.service.IdmHttpExecuteService;
import com.guosen.zebra.framework.starter.idm.http.service.impl.IdmHttpExecuteServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(IdmHttpProperties.class)
public class IdmHttpAutoConfiguration {

    @Bean
    public IdmHttpExecuteService idmHttpExecuteService(IdmHttpProperties idmHttpProperties) {
        return new IdmHttpExecuteServiceImpl(idmHttpProperties);
    }

}
