package com.guosen.zebra.framework.starter.idm.http.exception;

import org.apache.commons.lang3.StringUtils;

/**
 * 调用异常
 */
public class IdmHttpException extends RuntimeException {

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    public IdmHttpException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public IdmHttpException(String message, Throwable cause) {
        super(message, cause);
    }

    public IdmHttpException(Exception e) {
        super(e);
    }

    @Override
    public String getMessage() {
        return StringUtils.isNotEmpty(this.message) ? this.message : super.getMessage();
    }

}
