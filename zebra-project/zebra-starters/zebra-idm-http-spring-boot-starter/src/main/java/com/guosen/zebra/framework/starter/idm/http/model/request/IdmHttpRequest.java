package com.guosen.zebra.framework.starter.idm.http.model.request;

import lombok.Data;

/**
 * @className: IdmHttpRequest
 * @description: idm http接口请求类
 **/
@Data
public class IdmHttpRequest {
    /**
     * 分页大小
     */
    private String size;

    /**
     * 页数
     */
    private String page;

    /**
     * 开始时间戳
     */
    private String startTime;

    /**
     * 查询结果
     * 1 有效
     * 0 无效
     * 不传就全部
     */
    private Integer status;
}
