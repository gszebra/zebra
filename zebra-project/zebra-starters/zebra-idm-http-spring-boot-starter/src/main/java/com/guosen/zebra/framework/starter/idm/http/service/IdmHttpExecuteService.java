package com.guosen.zebra.framework.starter.idm.http.service;

import com.guosen.zebra.framework.starter.idm.http.model.request.IdmHttpRequest;
import com.guosen.zebra.framework.starter.idm.http.model.result.IdmHttpRespon;

/**
 * @className: IdmHttpExecuteService
 * @description: Idm http 接口定义
 **/
public interface IdmHttpExecuteService {

    /**
     * 查询账户列表
     * @param request
     * @return
     */
    IdmHttpRespon getAccountList(IdmHttpRequest request);

    /**
     * 查询部门列表
     * @param request
     * @return
     */
    IdmHttpRespon getDeptList(IdmHttpRequest request);
}
