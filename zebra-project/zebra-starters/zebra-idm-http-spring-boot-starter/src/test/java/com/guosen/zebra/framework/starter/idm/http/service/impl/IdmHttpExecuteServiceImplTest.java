package com.guosen.zebra.framework.starter.idm.http.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.guosen.zebra.framework.starter.idm.http.exception.IdmHttpException;
import com.guosen.zebra.framework.starter.idm.http.internal.IdmHttpClient;
import com.guosen.zebra.framework.starter.idm.http.model.request.IdmHttpRequest;
import com.guosen.zebra.framework.starter.idm.http.model.result.IdmHttpRespon;
import com.guosen.zebra.framework.starter.idm.http.properties.IdmHttpProperties;
import com.guosen.zebra.framework.starter.idm.http.utils.JsonUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class IdmHttpExecuteServiceImplTest {

    @Mock
    private IdmHttpProperties idmHttpProperties;

    @InjectMocks
    private IdmHttpExecuteServiceImpl idmHttpExecuteService;

    private IdmHttpRequest request;

    @Test
    void testGetAccountList_Success() throws Exception {
        String url = "http://localhost:8080" + "/esc-idm/api/v1/ext/account/list";
        when(idmHttpProperties.getUrl()).thenReturn("http://localhost:8080");

        try (MockedStatic<IdmHttpClient> mockedHttpClient = mockStatic(IdmHttpClient.class)) {
            mockedHttpClient.when(() -> IdmHttpClient.getHeaders(idmHttpProperties)).thenReturn(null);
            mockedHttpClient.when(() -> IdmHttpClient.post(eq(url), any(), eq(request))).thenReturn(getJsonNode());

            IdmHttpRespon response = idmHttpExecuteService.getAccountList(request);
            IdmHttpClient.post(eq(url), any(), eq(request));
            assertNotNull(response);
        }
    }

    @Test
    void testGetAccountList_Failure() throws Exception {
        String url = "http://localhost:8080" + "/esc-idm/api/v1/ext/account/list";
        when(idmHttpProperties.getUrl()).thenReturn("http://localhost:8080");

        try (MockedStatic<IdmHttpClient> mockedHttpClient = mockStatic(IdmHttpClient.class)) {
            mockedHttpClient.when(() -> IdmHttpClient.getHeaders(idmHttpProperties)).thenReturn(null);
            mockedHttpClient.when(() -> IdmHttpClient.post(eq(url), any(), eq(request))).thenThrow(new IdmHttpException(new RuntimeException()));
            IdmHttpException thrown = assertThrows(IdmHttpException.class, () -> {
                idmHttpExecuteService.getAccountList(request);
            });
            assertThat(thrown.getMessage(), is("查询账号列表错误！"));
        }
    }

    @Test
    void testGetDeptList_Success() throws Exception {
        String url = "http://localhost:8080" + "/esc-idm/api/v1/ext/org/list";
        when(idmHttpProperties.getUrl()).thenReturn("http://localhost:8080");

        try (MockedStatic<IdmHttpClient> mockedHttpClient = mockStatic(IdmHttpClient.class)) {
            mockedHttpClient.when(() -> IdmHttpClient.getHeaders(idmHttpProperties)).thenReturn(null);
            mockedHttpClient.when(() -> IdmHttpClient.post(eq(url), any(), eq(request))).thenReturn(getJsonNode());
            IdmHttpRespon response = idmHttpExecuteService.getDeptList(request);
            IdmHttpClient.post(eq(url), any(), eq(request));
            assertNotNull(response);
        }
    }

    @Test
    void testGetDeptList_Failure() throws Exception {
        String url = "http://localhost:8080" + "/esc-idm/api/v1/ext/org/list";
        when(idmHttpProperties.getUrl()).thenReturn("http://localhost:8080");

        try (MockedStatic<IdmHttpClient> mockedHttpClient = mockStatic(IdmHttpClient.class)) {
            mockedHttpClient.when(() -> IdmHttpClient.getHeaders(idmHttpProperties)).thenReturn(null);
            mockedHttpClient.when(() -> IdmHttpClient.post(eq(url), any(), eq(request))).thenThrow(new IdmHttpException(new RuntimeException()));

            IdmHttpException thrown = assertThrows(IdmHttpException.class, () -> {
                idmHttpExecuteService.getDeptList(request);
            });

            assertThat(thrown.getMessage(), is("查询部门列表错误！"));
        }
    }


    private static JsonNode getJsonNode() {
        try {
            String jsonStr = "{\"code\":0,\"msg\":\"success\",\"timeStamp\":1733475560749,\"data\":\"data\"}";
            return JsonUtil.getObjectMapper().readTree(jsonStr);
        } catch (Exception e) {
            return null;
        }
    }
}
