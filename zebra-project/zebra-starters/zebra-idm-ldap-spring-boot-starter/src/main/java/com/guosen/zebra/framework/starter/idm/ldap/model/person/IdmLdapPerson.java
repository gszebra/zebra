package com.guosen.zebra.framework.starter.idm.ldap.model.person;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * 员工信息
 */
@Data
public class IdmLdapPerson {

    /**
     * 账号(工号）
     */
    @JsonProperty("uid")
    private String number;

    /**
     * 姓名
     */
    @JsonProperty("cn")
    private String name;

    /**
     * 内网邮箱
     */
    @JsonProperty("mail")
    private String intranetMail;

    /**
     * 外网邮箱
     */
    @JsonProperty("InternetAddress")
    private String internetMail;

    /**
     * 部门编号
     */
    @JsonProperty("departmentNumber")
    private String deptId;

    /**
     * 别名
     */
    @JsonProperty("smart-alias")
    private String userAlias;

    /**
     * 是否离职，0：在职，1：离职
     */
    @JsonProperty("smart-status")
    private String status;

    /**
     * 员工类型，0：正式员工，1：外包人员，2：公共账号，3：辅助员工
     */
    @JsonProperty("smart-type")
    private String type;

    /**
     * 用户入职时间
     */
    @JsonProperty("customized-joinsysdate")
    @JsonFormat(pattern = "yyyyMMddHHmmss", timezone = "GMT+8")
    private Date customizedJoinsysdate;
}
