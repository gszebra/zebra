package com.guosen.zebra.framework.starter.idm.ldap.model.dept;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 部门信息
 */
@Data
public class IdmLdapDept {

    /**
     * 部门编码
     */
    @JsonProperty("o")
    private String number;

    /**
     * 部门名称
     */
    @JsonProperty("smart-shortname")
    private String deptName;

    /**
     * 部门全名称，从一级部门开始以/分隔
     */
    @JsonProperty("departmentName")
    private String deptFullName;

    /**
     * 上级部门编码
     */
    @JsonProperty("smart-parentid")
    private String parentNumber;

    /**
     * 状态：0-正常，1-禁用
     */
    @JsonProperty("smart-status")
    private String status;
}
