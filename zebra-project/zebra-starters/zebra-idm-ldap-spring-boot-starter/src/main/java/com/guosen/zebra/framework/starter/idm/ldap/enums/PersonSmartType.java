package com.guosen.zebra.framework.starter.idm.ldap.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 员工类型
 */
@Getter
@AllArgsConstructor
public enum PersonSmartType {

    REGULAR("0", "正式员工"),
    OUTSOURCED("1", "外包人员"),
    PUBLIC("2", "公共账号"),
    SUPPORTED("3", "辅助员工"),
    ;
    private final String code;
    private final String description;
}
