package com.guosen.zebra.framework.starter.idm.ldap.model.dept;

import com.guosen.zebra.framework.starter.idm.ldap.enums.IdmLdapRspCode;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * 部门信息响应
 */
@Data
@AllArgsConstructor
public class IdmDeptListRsp {

    /**
     * 状态码, 状态码, 0表示成功，其它值表示失败
     * {@link com.guosen.zebra.framework.starter.idm.ldap.enums.IdmLdapRspCode}
     */
    private Integer code;

    /**
     * 返回信息
     */
    private String message;

    /**
     * 数据
     */
    private List<IdmLdapDept> data;

    public IdmDeptListRsp(IdmLdapRspCode code, List<IdmLdapDept> data) {
        this.code = code.getCode();
        this.message = code.getMessage();
        this.data = data;
    }
}
