package com.guosen.zebra.framework.starter.idm.ldap.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IdmLdapRspCode {
    SUCCESS(0, "success"),
    FAILED(1, "error"),

    ;
    /**
     * 错误码
     */
    private final Integer code;

    /**
     * 错误信息
     */
    private final String message;
}
