package com.guosen.zebra.framework.starter.idm.ldap.service;

import com.guosen.zebra.framework.starter.idm.ldap.query.IdmLdapPersonQuery;
import com.guosen.zebra.framework.starter.idm.ldap.model.dept.IdmDeptListRsp;
import com.guosen.zebra.framework.starter.idm.ldap.model.person.IdmPersonListRsp;

/**
 * IDM信息服务接口
 */
public interface IdmLdapService {

    /**
     * 查询所有正常状态的部门
     */
    IdmDeptListRsp getLdapDeptList();

    /**
     * 查询所有在职的正式员工和辅助员工
     */
    IdmPersonListRsp getLdapPersonList();

    /**
     * 查询符合条件的员工信息
     * @param query 查询条件
     */
    IdmPersonListRsp getLdapPersonListWithConditions(IdmLdapPersonQuery query);
}
