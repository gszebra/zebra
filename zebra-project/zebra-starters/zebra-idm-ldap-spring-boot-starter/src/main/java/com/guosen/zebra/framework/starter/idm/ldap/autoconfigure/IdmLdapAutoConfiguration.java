package com.guosen.zebra.framework.starter.idm.ldap.autoconfigure;


import com.guosen.zebra.framework.starter.idm.ldap.properties.IdmLdapProperties;
import com.guosen.zebra.framework.starter.idm.ldap.service.IdmLdapExecuteService;
import com.guosen.zebra.framework.starter.idm.ldap.service.IdmLdapService;
import com.guosen.zebra.framework.starter.idm.ldap.service.impl.IdmLdapExecuteServiceImpl;
import com.guosen.zebra.framework.starter.idm.ldap.service.impl.IdmLdapServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(IdmLdapProperties.class)
public class IdmLdapAutoConfiguration {

    @Bean
    public IdmLdapExecuteService idmLdapExecuteService(IdmLdapProperties idmLdapProperties) {
        return new IdmLdapExecuteServiceImpl(idmLdapProperties);
    }

    @Bean
    public IdmLdapService idmLdapService(IdmLdapProperties idmLdapProperties, IdmLdapExecuteService idmLdapExecuteService) {
        return new IdmLdapServiceImpl(idmLdapProperties, idmLdapExecuteService);
    }
}
