package com.guosen.zebra.framework.starter.idm.ldap.constants;

/**
 * Idm Ldap 搜索相关常量
 */
public final class IdmLdapQuery {

    private IdmLdapQuery() {}

    public static final String OR = "|";

    /**
     * 部门-默认基础搜索目录
     */
    public static final String DEPT_DEFAULT_BASE = "ou=administration,ou=organizations,o=guosen.com.cn,o=isp";

    /**
     * 部门-默认搜索过滤器
     */
    public static final String DEPT_DEFAULT_FILTER = "(&(objectclass=*)(o=*)(smart-status=0))";

    /**
     * 员工-默认基础搜索目录
     */
    public static final String PERSON_DEFAULT_BASE = "ou=People,o=guosen.com.cn,o=isp";

    /**
     * 员工-默认搜索过滤器
     */
    public static final String PERSON_DEFAULT_FILTER = "(&(objectclass=*)(cn=*)(smart-status=0)(|(smart-type=0)(smart-type=3)))";

    /**
     * 员工信息过滤器模板
     */
    public static final String PERSON_FILTER_TEMPLATE = "(&(objectclass=*)(cn=*)%s%s)";

    /**
     * 存在smart-status则为true
     */
    public static final String PERSON_STATUS_EXISTS = "(smart-status=*)";

    /**
     * 存在smart-type则为true
     */
    public static final String PERSON_TYPE_EXISTS = "(smart-type=*)";

    /**
     * smart-status条件前缀
     */
    public static final String SMART_STATUS_PREFIX = "smart-status=";

    /**
     * smart-type条件前缀
     */
    public static final String SMART_TYPE_PREFIX = "smart-type=";
}
