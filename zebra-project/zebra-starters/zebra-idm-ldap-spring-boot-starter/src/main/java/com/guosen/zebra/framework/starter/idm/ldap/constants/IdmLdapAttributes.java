package com.guosen.zebra.framework.starter.idm.ldap.constants;

/**
 * 属性名常量
 */
public final class IdmLdapAttributes {

    private IdmLdapAttributes() {}
    public static final String SMART_CIPHER_TEXT = "smart-ciphertext";

    public static final String SMART_SIGN = "smart-sign";

    /**
     * 部门信息返回字段
     */
    public static final String[] DEPT_COLUMNS =
            {"departmentName", "smart-parentid", "smart-shortname", "smart-status", "o"};

    /**
     * 人员信息返回字段
     */
    public static final String[] PERSON_COLUMNS =
            {"cn","smart-alias", "customized-jobname", "mail", "InternetAddress", "customized-joinsysdate", "departmentNumber","uid", "smart-status", "smart-type"};

}
