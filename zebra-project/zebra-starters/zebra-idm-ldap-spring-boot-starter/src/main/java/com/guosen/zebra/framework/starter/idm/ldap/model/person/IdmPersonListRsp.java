package com.guosen.zebra.framework.starter.idm.ldap.model.person;

import com.guosen.zebra.framework.starter.idm.ldap.enums.IdmLdapRspCode;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * 员工信息响应
 */
@Data
@AllArgsConstructor
public class IdmPersonListRsp {
    /**
     * 状态码, 0表示成功，其它值表示失败
     * {@link com.guosen.zebra.framework.starter.idm.ldap.enums.IdmLdapRspCode}
     */
    private Integer code;

    /**
     * 返回信息
     */
    private String message;

    /**
     * 数据
     */
    private List<IdmLdapPerson> data;

    public IdmPersonListRsp(IdmLdapRspCode idmLdapRspCode, List<IdmLdapPerson> data) {
        this.code = idmLdapRspCode.getCode();
        this.message = idmLdapRspCode.getMessage();
        this.data = data;
    }
}