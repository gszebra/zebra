package com.guosen.zebra.framework.starter.idm.ldap.service;

import java.util.List;
import java.util.Map;

/**
 * 实际查询接口
 */
public interface IdmLdapExecuteService {

    /**
     * 执行查询
     * @param searchBase 搜索基础目录
     * @param searchFilter 搜索过滤器
     * @param returnedAttrs 返回属性集
     */
    List<Map<String, String>> consumeServiceWithCustomConfiguration(String searchBase, String searchFilter,
                                     String[] returnedAttrs) throws Exception;
}
