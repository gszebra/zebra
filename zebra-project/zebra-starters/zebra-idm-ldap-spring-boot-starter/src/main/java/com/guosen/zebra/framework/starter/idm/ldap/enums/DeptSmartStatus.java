package com.guosen.zebra.framework.starter.idm.ldap.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 部门状态
 */
@Getter
@AllArgsConstructor
public enum DeptSmartStatus {

    NORMAL("0", "正常"),
    DISABLED("1", "禁用"),
    ;
    private final String code;
    private final String description;
}
