package com.guosen.zebra.framework.starter.idm.ldap.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.guosen.zebra.framework.starter.idm.ldap.constants.IdmLdapQuery.*;

@Data
@ConfigurationProperties(prefix = "zebra.idm.ldap")
public class IdmLdapProperties {

    /**
     * 用户名
     */
    private String user;

    /**
     * 密码
     */
    private String pwd;

    /**
     * url
     */
    private String url;

    /**
     * 部门信息的基础搜索目录
     */
    private String deptDefaultBase = DEPT_DEFAULT_BASE;

    /**
     * 部门信息的搜索过滤器
     */
    private String deptDefaultFilter = DEPT_DEFAULT_FILTER;

    /**
     * 员工信息的基础搜索目录
     */
    private String personDefaultBase = PERSON_DEFAULT_BASE;

    /**
     * 员工信息的搜索过滤器
     */
    private String personDefaultFilter = PERSON_DEFAULT_FILTER;

    /**
     * 员工信息过滤器模板（需要指定smart-status或smart-type时使用）
     */
    private String personFilterTemplate = PERSON_FILTER_TEMPLATE;
}
