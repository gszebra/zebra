package com.guosen.zebra.framework.starter.idm.ldap.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 员工状态
 */
@Getter
@AllArgsConstructor
public enum PersonSmartStatus {

    EMPLOYED("0", "在职"),
    RESIGNED("1", "离职"),
    ;
    private final String code;
    private final String description;
}
