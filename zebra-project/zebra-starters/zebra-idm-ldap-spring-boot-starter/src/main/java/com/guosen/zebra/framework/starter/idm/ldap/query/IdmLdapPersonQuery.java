package com.guosen.zebra.framework.starter.idm.ldap.query;

import lombok.Data;

import java.util.List;

/**
 * 员工信息查询条件
 */
@Data
public class IdmLdapPersonQuery {

    /**
     * 员工状态，为空默认查(smart-status=*)，取值可参考
     * {@link com.guosen.zebra.framework.starter.idm.ldap.enums.PersonSmartStatus}
     */
    private List<String> smartStatuses;

    /**
     * 员工类型，为空默认查(smart-type=*)，取值可参考
     * {@link com.guosen.zebra.framework.starter.idm.ldap.enums.PersonSmartType}
     */
    private List<String> smartTypes;
}
