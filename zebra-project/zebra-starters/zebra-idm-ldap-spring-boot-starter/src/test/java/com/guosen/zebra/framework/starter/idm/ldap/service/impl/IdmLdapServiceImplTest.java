package com.guosen.zebra.framework.starter.idm.ldap.service.impl;

import com.guosen.zebra.framework.starter.idm.ldap.constants.IdmLdapAttributes;
import com.guosen.zebra.framework.starter.idm.ldap.enums.IdmLdapRspCode;
import com.guosen.zebra.framework.starter.idm.ldap.model.dept.IdmDeptListRsp;
import com.guosen.zebra.framework.starter.idm.ldap.model.person.IdmPersonListRsp;
import com.guosen.zebra.framework.starter.idm.ldap.properties.IdmLdapProperties;
import com.guosen.zebra.framework.starter.idm.ldap.query.IdmLdapPersonQuery;
import com.guosen.zebra.framework.starter.idm.ldap.service.IdmLdapExecuteService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.guosen.zebra.framework.starter.idm.ldap.constants.IdmLdapQuery.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 * IdmLdapServiceImplTest
 */
@ExtendWith(MockitoExtension.class)
public class IdmLdapServiceImplTest {

    @Mock
    private IdmLdapProperties idmLdapProperties;

    @Mock
    private IdmLdapExecuteService idmLdapExecuteService;

    @InjectMocks
    private IdmLdapServiceImpl idmLdapService;

    @Test
    void testGetLdapDeptList() throws Exception {
        when(idmLdapProperties.getDeptDefaultBase()).thenReturn(PERSON_DEFAULT_BASE);
        when(idmLdapExecuteService.consumeServiceWithCustomConfiguration(
                PERSON_DEFAULT_BASE, DEPT_DEFAULT_FILTER, IdmLdapAttributes.DEPT_COLUMNS))
                .thenReturn(new ArrayList<>());

        IdmDeptListRsp deptListRsp = idmLdapService.getLdapDeptList();
        assertEquals(IdmLdapRspCode.SUCCESS.getCode(), deptListRsp.getCode());
    }

    @Test
    void testGetLdapDeptListWithFailed() throws Exception {
        when(idmLdapProperties.getDeptDefaultBase()).thenReturn(PERSON_DEFAULT_BASE);
        when(idmLdapExecuteService.consumeServiceWithCustomConfiguration(
                PERSON_DEFAULT_BASE, DEPT_DEFAULT_FILTER, IdmLdapAttributes.DEPT_COLUMNS))
                .thenThrow(new Exception("exception"));

        IdmDeptListRsp deptListRsp = idmLdapService.getLdapDeptList();
        assertEquals(IdmLdapRspCode.FAILED.getCode(), deptListRsp.getCode());
    }

    @Test
    void testGetLdapPersonList() throws Exception {
        when(idmLdapProperties.getPersonDefaultBase()).thenReturn(PERSON_DEFAULT_BASE);
        when(idmLdapProperties.getPersonDefaultFilter()).thenReturn(DEPT_DEFAULT_FILTER);
        when(idmLdapExecuteService.consumeServiceWithCustomConfiguration(
                PERSON_DEFAULT_BASE, DEPT_DEFAULT_FILTER, IdmLdapAttributes.PERSON_COLUMNS))
                .thenReturn(new ArrayList<>());

        IdmPersonListRsp personListRsp = idmLdapService.getLdapPersonList();
        assertEquals(IdmLdapRspCode.SUCCESS.getCode(), personListRsp.getCode());
    }

    @Test
    void testGetLdapPersonListWithFailed() throws Exception {
        when(idmLdapProperties.getPersonDefaultBase()).thenReturn(PERSON_DEFAULT_BASE);
        when(idmLdapExecuteService.consumeServiceWithCustomConfiguration(
                PERSON_DEFAULT_BASE, PERSON_DEFAULT_FILTER, IdmLdapAttributes.PERSON_COLUMNS))
                .thenThrow(new Exception("exception"));

        IdmPersonListRsp personListRsp = idmLdapService.getLdapPersonList();
        assertEquals(IdmLdapRspCode.FAILED.getCode(), personListRsp.getCode());
    }

    @Test
    void testGetLdapPersonListWithConditions() throws Exception {
        IdmLdapPersonQuery personQuery = getPersonQuery();

        when(idmLdapProperties.getPersonDefaultBase()).thenReturn(PERSON_DEFAULT_BASE);
        when(idmLdapProperties.getPersonFilterTemplate()).thenReturn("person_filter_template");
        when(idmLdapExecuteService.consumeServiceWithCustomConfiguration(
                PERSON_DEFAULT_BASE, "person_filter_template", IdmLdapAttributes.PERSON_COLUMNS))
                .thenReturn(new ArrayList<>());

        IdmPersonListRsp personListRsp = idmLdapService.getLdapPersonListWithConditions(personQuery);
        assertEquals(IdmLdapRspCode.SUCCESS.getCode(), personListRsp.getCode());
    }

    private static IdmLdapPersonQuery getPersonQuery() {
        IdmLdapPersonQuery idmLdapPersonQuery = new IdmLdapPersonQuery();

        List<String> statusList = new ArrayList<>();
        statusList.add("EMPLOYED");
        idmLdapPersonQuery.setSmartStatuses(statusList);

        List<String> typeList = new ArrayList<>();
        typeList.add("REGULAR");
        typeList.add("SUPPORTED");
        idmLdapPersonQuery.setSmartTypes(typeList);
        return idmLdapPersonQuery;
    }
}
