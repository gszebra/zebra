package com.guosen.zebra.framework.starter.http.log.vo;

import lombok.Data;

import java.util.Map;

/**
 * Biz请求日志对象
 */
@Data
public class HttpBizReqLogVO {

    /**
     * 请求时间，格式如：2021-06-15 13:07:24.270
     */
    private String t;

    /**
     * 跟踪链ID
     */
    private String traceId;

    /**
     * HTTP method
     */
    private String method;

    /**
     * 入口请求URL
     */
    private String path;

    /**
     * 数据类型
     */
    private String contentType;

    /**
     * 请求参数
     */
    private ReqParams reqParams;

    /**
     * 服务端地址，也就是本机地址
     */
    private String serverAddr;

    /**
     * 请求方地址
     */
    private String remoteAddr;

    @Data
    public static class ReqParams {

        /**
         * 路径参数
         */
        private Map<String, Object> path;

        /**
         * Query参数
         */
        private Map<String, Object> query;

        /**
         * 请求body
         */
        private String body;
    }
}
