package com.guosen.zebra.framework.starter.http.log.autoconfigure;

import com.guosen.zebra.framework.starter.http.log.filter.InterfaceLogFilter;
import com.guosen.zebra.framework.starter.http.log.properties.HttpLogProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(value = "zebra.http-log.enabled", matchIfMissing = true)
@EnableConfigurationProperties(HttpLogProperties.class)
public class HttpLogAutoConfiguration implements WebMvcConfigurer {

    private final HttpLogProperties httpLogProperties;

    public HttpLogAutoConfiguration(HttpLogProperties httpLogProperties) {
        this.httpLogProperties = httpLogProperties;
    }

    @Bean
    public FilterRegistrationBean<InterfaceLogFilter> interfaceLogFilterRegistration() {
        FilterRegistrationBean<InterfaceLogFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setOrder(2);
        filterRegistrationBean.setFilter(new InterfaceLogFilter(httpLogProperties));
        return filterRegistrationBean;
    }
}
