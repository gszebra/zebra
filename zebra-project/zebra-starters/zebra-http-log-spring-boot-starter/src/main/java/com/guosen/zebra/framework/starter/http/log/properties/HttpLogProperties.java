package com.guosen.zebra.framework.starter.http.log.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Http日志记录配置类
 */
@Data
@ConfigurationProperties(prefix = "zebra.http-log")
public class HttpLogProperties {

    /**
     * 是否启用日志记录功能，默认为true
     */
    private boolean enabled = true;

    private Acc acc = new Acc();

    private Biz biz = new Biz();

    @Data
    public static class Acc {

        /**
         * acc log 开关
         */
        private boolean enabled = true;
    }

    @Data
    public static class Biz {

        /**
         * biz请求开关
         */
        private boolean reqEnabled = true;

        /**
         * biz响应开关
         */
        private boolean rspEnabled = true;
    }
}
