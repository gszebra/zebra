package com.guosen.zebra.framework.starter.http.log.constants;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 日志记录常量类
 */
public class HttpLogConstant {

    private HttpLogConstant() {}

    /**
     * requestBody & responseBody 最大记录长度
     */
    public static final int MAX_PAYLOAD_LENGTH = 2000;

    /**
     * 过滤列表，对于列表中列出的contentType，日志记录时不记录请求/响应体
     */
    public static final List<String> CONTENT_TYPE_IGNORE_LIST = Collections.unmodifiableList(
            Arrays.asList("image/gif", "image/jpeg", "image/png", "application/octet-stream")
    );

    /**
     * request属性名，用于关联请求开始时间
     */
    public static final String ZEBRA_HTTP_LOG_START_TIME = "_zebraHttpLogStartTime";

    /**
     * request属性名，用于关联结束开始时间
     */
    public static final String ZEBRA_HTTP_LOG_END_TIME = "_zebraHttpLogEndTime";
}
