package com.guosen.zebra.framework.starter.http.log.vo;

import lombok.Data;

/**
 * Biz响应日志对象
 */
@Data
public class HttpBizRspLogVO {

    /**
     * 响应时间，格式如：2021-06-15 13:07:24.270
     */
    private String t;

    /**
     * 跟踪链ID
     */
    private String traceId;

    /**
     * HTTP method
     */
    private String method;

    /**
     * 入口请求URL
     */
    private String path;

    /**
     * 数据类型
     */
    private String contentType;

    /**
     * HTTP响应状态码
     */
    private String httpStatusCode;

    /**
     * 返回body
     */
    private String rsp;

    /**
     * 服务端地址，也就是本机地址
     */
    private String serverAddr;

    /**
     * 请求方地址
     */
    private String remoteAddr;
}
