package com.guosen.zebra.framework.starter.ngx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 创建文件夹结果
 */
@Data
public class MkdirResult {
    @JsonProperty("fs_id")
    private String fsId;

    private String path;

    @JsonProperty("create_time")
    private String createTime;
}
