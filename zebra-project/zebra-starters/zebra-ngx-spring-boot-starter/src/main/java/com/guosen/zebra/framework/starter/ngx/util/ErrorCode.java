package com.guosen.zebra.framework.starter.ngx.util;

public class ErrorCode {
    public static class List {
        /**
         * 找不到文件
         */
        public static final String FILE_NOT_FOUND = "400";
    }
}
