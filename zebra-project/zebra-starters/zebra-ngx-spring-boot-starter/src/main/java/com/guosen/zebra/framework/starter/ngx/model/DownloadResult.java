package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

/**
 * 下载结果
 */
@Data
public class DownloadResult {

    /**
     * 文件内容
     */
    private byte[] fileContent;

}
