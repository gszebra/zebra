package com.guosen.zebra.framework.starter.ngx.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 安渡配置
 */
@ConfigurationProperties(prefix = "zebra.ngx")
@Data
public class NgxConfig {
    /**
     * 安渡系统地址
     */
    private String address;

    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;
}
