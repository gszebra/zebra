package com.guosen.zebra.framework.starter.ngx.internal.impl;



import com.fasterxml.jackson.databind.JsonNode;
import com.guosen.zebra.framework.starter.ngx.exception.NgxException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class UniNgxToken {
    /**
     * 获取token路径
     */
    private static final String TOKEN_URL = "/UniExServices/restfulapi/token";

    public String execute(String account, String password, String address) throws NgxException {
        String fullUrl = address + TOKEN_URL;

        Map<String, String> parameters = new HashMap<>();
        UniNgxUtil.addParameter(parameters,"account", account);
        UniNgxUtil.addParameter(parameters, "password", password);

        JsonNode result = UniNgxRestfulClient.post(fullUrl, Collections.emptyMap(), parameters);
        UniNgxUtil.raiseExceptionIfExist(result);

        JsonNode tokenNode = result.get("token");
        if (tokenNode == null) {
            throw new NgxException("500", "获取token失败");
        }

        return tokenNode.asText();
    }
}
