package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

/**
 * 移动文件参数
 */
@Data
public class MoveParameter {
    /**
     * 路径
     */
    private String path;

    /**
     * 移动的源文件路径
     */
    private String from;

    /**
     * true:移动文件夹，false：移动文件。
     */
    private Boolean isDir;

    /**
     * 目录类型
     */
    private Location location = Location.MY_DOC;

    public static class Builder {
        private MoveParameter moveParameter = new MoveParameter();

        public Builder path(String path) {
            moveParameter.path = path;
            return this;
        }

        public Builder from(String from) {
            moveParameter.from = from;
            return this;
        }

        public Builder isDir(boolean isDir) {
            moveParameter.isDir = isDir;
            return this;
        }

        public Builder location(Location location) {
            moveParameter.location = location;
            return this;
        }

        public MoveParameter build() {
            return moveParameter;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
