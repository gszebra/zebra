package com.guosen.zebra.framework.starter.ngx.autoconfigure;

import com.guosen.zebra.framework.starter.ngx.api.NgxClient;
import com.guosen.zebra.framework.starter.ngx.api.impl.NgxClientImpl;
import com.guosen.zebra.framework.starter.ngx.config.NgxConfig;
import com.guosen.zebra.framework.starter.ngx.internal.UniNgxService;
import com.guosen.zebra.framework.starter.ngx.internal.impl.UniNgxServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(NgxConfig.class)
public class NgxAutoConfiguration {

    @Bean
    public UniNgxService uniNgxService() {
        return new UniNgxServiceImpl();
    }

    @Bean
    public NgxClient uniNgxClient(UniNgxService uniNgxService,
                                  NgxConfig ngxConfig) {
        return new NgxClientImpl(uniNgxService, ngxConfig);
    }
}
