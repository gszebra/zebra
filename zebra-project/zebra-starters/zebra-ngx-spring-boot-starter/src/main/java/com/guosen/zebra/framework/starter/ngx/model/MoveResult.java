package com.guosen.zebra.framework.starter.ngx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MoveResult {
    private String path;

    @JsonProperty("create_time")
    private String createTime;

    @JsonProperty("fs_id")
    private String fsId;
}
