package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

@Data
public class DownloadParameter {
    private String path;

    private Location location;

    private Boolean isDir;

    public static class Builder {
        private DownloadParameter downloadParameter = new DownloadParameter();

        public Builder path(String path) {
            downloadParameter.path = path;
            return this;
        }

        public Builder location(Location location) {
            downloadParameter.location = location;
            return this;
        }

        public Builder isDir(boolean isDir) {
            downloadParameter.isDir = isDir;
            return this;
        }

        public DownloadParameter build() {
            return downloadParameter;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

}
