package com.guosen.zebra.framework.starter.ngx.internal.impl;


import com.guosen.zebra.framework.starter.ngx.exception.NgxException;
import com.guosen.zebra.framework.starter.ngx.internal.UniNgxService;
import com.guosen.zebra.framework.starter.ngx.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UniNgxServiceImpl implements UniNgxService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UniNgxServiceImpl.class);

    private UniNgxToken uniNgxToken = new UniNgxToken();

    private UniNgxList uniNgxList = new UniNgxList();

    private UniNgxUpload uniNgxUpload = new UniNgxUpload();

    private UniNgxDownload uniNgxDownload = new UniNgxDownload();

    private UniNgxMkdir uniNgxMkdir = new UniNgxMkdir();

    private UniNgxMove uniNgxMove = new UniNgxMove();

    private UniNgxDelete uniNgxDelete = new UniNgxDelete();

    private UniNgxDestroy uniNgxDestroy = new UniNgxDestroy();

    @Override
    public String token(String account, String password, String address) throws NgxException {
        return uniNgxToken.execute(account, password, address);
    }

    @Override
    public ListResult list(ListParameter listParameter, InvokeContext invokeContext) throws NgxException {
        return uniNgxList.execute(listParameter, invokeContext);
    }

    @Override
    public UploadResult upload(UploadParameter uploadParameter, InvokeContext invokeContext) throws NgxException {
        return uniNgxUpload.execute(uploadParameter, invokeContext);
    }

    @Override
    public DownloadResult download(DownloadParameter downloadParameter, InvokeContext invokeContext) throws NgxException {
        return uniNgxDownload.execute(downloadParameter, invokeContext);
    }

    @Override
    public MkdirResult mkdir(MkdirParameter mkdirParameter, InvokeContext invokeContext) throws NgxException {
        return uniNgxMkdir.execute(mkdirParameter, invokeContext);
    }

    @Override
    public MoveResult move(MoveParameter moveParameter, InvokeContext invokeContext) throws NgxException {
        return uniNgxMove.execute(moveParameter, invokeContext);
    }

    @Override
    public void delete(DeleteParameter deleteParameter, InvokeContext invokeContext) throws NgxException {
        uniNgxDelete.execute(deleteParameter, invokeContext);
    }

    @Override
    public void destroy(DestroyParameter destroyParameter, InvokeContext invokeContext) throws NgxException {
        uniNgxDestroy.execute(destroyParameter, invokeContext);
    }
}
