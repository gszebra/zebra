package com.guosen.zebra.framework.starter.ngx.internal.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.guosen.zebra.framework.starter.ngx.exception.NgxException;
import com.guosen.zebra.framework.starter.ngx.model.InvokeContext;
import com.guosen.zebra.framework.starter.ngx.model.MkdirParameter;
import com.guosen.zebra.framework.starter.ngx.model.MkdirResult;
import com.guosen.zebra.framework.starter.ngx.util.UrlEncodeDecoder;

import java.util.HashMap;
import java.util.Map;

public class UniNgxMkdir {

    private static final String MKDIR_PATH = "/UniExServices/restfulapi/%s%s";

    public MkdirResult execute(MkdirParameter mkdirParameter, InvokeContext invokeContext) throws NgxException {
        String urlEncodeAccount = UrlEncodeDecoder.encode(invokeContext.getAccount());
        String urlEncodePath = UniNgxUtil.urlEncodePath(mkdirParameter.getPath());
        String mkdirPath = String.format(MKDIR_PATH, urlEncodeAccount, urlEncodePath);

        String url = invokeContext.getAddress() + mkdirPath;

        Map<String, String> headers = UniNgxUtil.buildHeaders(invokeContext);
        Map<String, String> parameters = buildParameter(mkdirParameter);

        JsonNode result = UniNgxRestfulClient.get(url, headers, parameters);
        UniNgxUtil.raiseExceptionIfExist(result);

        try {
            return UniNgxUtil.getObjectMapper().treeToValue(result, MkdirResult.class);
        } catch (JsonProcessingException e) {
            throw new NgxException(e);
        }
    }

    private Map<String, String> buildParameter(MkdirParameter mkdirParameter) {
        Map<String, String> parameters = new HashMap<>();
        UniNgxUtil.addParameter(parameters, "method", "mkdir");

        return parameters;
    }
}
