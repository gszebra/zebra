package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

@Data
public class UploadParameter {

    /**
     * 上传文件路径
     */
    private String path;

    /**
     * 文件内容
     */
    private byte[] fileContent;

    /**
     * 目录类别
     */
    private Location location = Location.MY_DOC;

    /**
     * 值为true时，当不存在上传路径时，递归创建路径。
     */
    private Boolean mkdirs;

    public static class Builder {
        private UploadParameter uploadParameter = new UploadParameter();

        public Builder path(String path) {
            this.uploadParameter.path = path;
            return this;
        }

        public Builder fileContent(byte[] fileContent) {
            this.uploadParameter.fileContent = fileContent;
            return this;
        }

        public Builder location(Location location) {
            this.uploadParameter.location = location;
            return this;
        }

        public Builder mkdirs(boolean mkdirs) {
            this.uploadParameter.mkdirs = mkdirs;
            return this;
        }

        public UploadParameter build() {
            return uploadParameter;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

}
