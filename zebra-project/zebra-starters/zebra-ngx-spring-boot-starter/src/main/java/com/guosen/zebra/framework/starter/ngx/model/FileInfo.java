package com.guosen.zebra.framework.starter.ngx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 文件信息
 */
@Data
public class FileInfo {
    /**
     * 文件（夹）的唯一标识id
     */
    @JsonProperty("fs_id")
    private String fsId;

    /**
     * true表示文件夹；false表示文件
     */
    @JsonProperty("is_dir")
    private boolean isDir;

    /**
     * 文件或文件夹的绝对路径
     */
    @JsonProperty("path")
    private String path;

    /**
     * 文件大小（byte)
     */
    private int size;

    /**
     * 文件（夹）进入回收站时间（即被删除时间）
     */
    @JsonProperty("create_time")
    private String createTime;

    /**
     * 文件的MD5值
     */
    @JsonProperty("MD5")
    private String md5;
}
