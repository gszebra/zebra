package com.guosen.zebra.framework.starter.ngx.internal.impl;



import com.fasterxml.jackson.databind.JsonNode;
import com.guosen.zebra.framework.starter.ngx.exception.NgxException;
import com.guosen.zebra.framework.starter.ngx.model.DeleteParameter;
import com.guosen.zebra.framework.starter.ngx.model.InvokeContext;
import com.guosen.zebra.framework.starter.ngx.util.UrlEncodeDecoder;

import java.util.HashMap;
import java.util.Map;

public class UniNgxDelete {

    /**
     * 删除文件请求路径
     */
    private static final String DELETE_PATH = "/UniExServices/restfulapi/%s%s";

    public void execute(DeleteParameter deleteParameter, InvokeContext invokeContext) throws NgxException {
        String urlEncodeAccount = UrlEncodeDecoder.encode(invokeContext.getAccount());
        String urlEncodePath = UniNgxUtil.urlEncodePath(deleteParameter.getPath());
        String url = invokeContext.getAddress() + String.format(DELETE_PATH, urlEncodeAccount, urlEncodePath);

        Map<String, String> parameters = buildParameters(deleteParameter);
        Map<String, String> headers = UniNgxUtil.buildHeaders(invokeContext);

        JsonNode result = UniNgxRestfulClient.get(url, headers, parameters);
        UniNgxUtil.raiseExceptionIfExist(result);
    }


    private Map<String, String> buildParameters(DeleteParameter deleteParameter) {
        Map<String, String> parameters = new HashMap<>();
        UniNgxUtil.addParameter(parameters, "method", "delete");
        UniNgxUtil.addParameter(parameters, "location", deleteParameter.getLocation());
        UniNgxUtil.addParameter(parameters, "is_dir", deleteParameter.getIsDir());

        return parameters;
    }
}
