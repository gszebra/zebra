package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

@Data
public class InvokeContext {
    private String account;

    private String token;

    private String address;
}
