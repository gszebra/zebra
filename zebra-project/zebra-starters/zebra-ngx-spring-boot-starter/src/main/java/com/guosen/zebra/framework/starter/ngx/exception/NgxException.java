package com.guosen.zebra.framework.starter.ngx.exception;

/**
 * 安渡系统调用异常
 */
public class NgxException extends Exception {
    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    public NgxException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public NgxException(Exception e) {
        super(e);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
