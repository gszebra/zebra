package com.guosen.zebra.framework.starter.ngx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 上传文件结果
 */
@Data
public class UploadResult {
    /**
     * 文件的唯一标识id
     */
    @JsonProperty("fs_id")
    private String fsId;

    /**
     * 文件或文件夹的绝对路径
     */
    private String path;

    /**
     * 文件大小(byte)
     */
    private int size;

    /**
     * 文件或文件夹创建时间
     */
    @JsonProperty("create_time")
    private String createTime;

    /**
     * 文件的MD5值
     */
    @JsonProperty("MD5")
    private String md5;
}
