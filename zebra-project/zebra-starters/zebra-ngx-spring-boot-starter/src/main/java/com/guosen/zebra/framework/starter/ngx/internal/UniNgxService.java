package com.guosen.zebra.framework.starter.ngx.internal;


import com.guosen.zebra.framework.starter.ngx.exception.NgxException;
import com.guosen.zebra.framework.starter.ngx.model.*;

/**
 * 安渡文件接口
 */
public interface UniNgxService {
    /**
     * 身份验证
     * @param account   账号
     * @param password  加密后的密码
     * @param address   安渡地址
     * @return  token
     * @throws NgxException 调用失败
     */
    String token(String account, String password, String address) throws NgxException;

    /**
     * 获取文件列表
     * @param listParameter 查询参数
     * @param invokeContext 上下文
     * @return  获取文件列表结果
     */
    ListResult list(ListParameter listParameter, InvokeContext invokeContext) throws NgxException;

    /**
     * 上传文件
     * @param uploadParameter   上传文件参数
     * @return  上传文件结果
     * @param invokeContext 上下文
     * @throws NgxException
     */
    UploadResult upload(UploadParameter uploadParameter, InvokeContext invokeContext) throws NgxException;

    /**
     * 下载文件
     * @param downloadParameter 下载文件参数
     * @return  下载文件结果
     * @param invokeContext 上下文
     * @throws NgxException
     */
    DownloadResult download(DownloadParameter downloadParameter, InvokeContext invokeContext) throws NgxException;

    /**
     * 创建文件夹
     * @param mkdirParameter    创建文件夹参数
     * @param invokeContext 上下文
     * @return  创建文件夹结果
     * @throws NgxException
     */
    MkdirResult mkdir(MkdirParameter mkdirParameter, InvokeContext invokeContext) throws NgxException;

    /**
     * 移动文件（夹）
     * @param moveParameter 移动文件（夹）参数
     * @return  移动文件结果
     * @param invokeContext 上下文
     * @throws NgxException
     */
    MoveResult move(MoveParameter moveParameter, InvokeContext invokeContext) throws NgxException;

    /**
     * 删除文件（夹）
     * @param deleteParameter   删除文件（夹）请求参数
     * @param invokeContext 上下文
     * @throws NgxException
     */
    void delete(DeleteParameter deleteParameter, InvokeContext invokeContext) throws NgxException;

    /**
     * 彻底删除文件（夹）
     * @param destroyParameter 彻底删除文件（夹）参数
     * @param invokeContext 上下文
     * @throws NgxException
     */
    void destroy(DestroyParameter destroyParameter, InvokeContext invokeContext) throws NgxException;
}
