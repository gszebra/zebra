package com.guosen.zebra.framework.starter.ngx.util;

import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * URL编码解码器
 */
@Slf4j
public final class UrlEncodeDecoder {

    private UrlEncodeDecoder(){}

    public static String encode(String originalString) {
        String encodeStr = null;
        try {
            encodeStr = URLEncoder.encode(originalString, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            // 不可能不支持的
            log.error(e.getMessage(), e);
        }

        return encodeStr;
    }
}
