package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

@Data
public class ListParameter {

    /**
     * 排序
     */
    public enum SortBy {

        /**
         * 按创建时间，升序
         */
        TIME("time"),

        /**
         * 按文件大小，升序
         */
        SIZE("size"),

        /**
         * 按文件名称，升序
         */
        NAME("name"),

        /**
         * 按创建时间，降序
         */
        RTIME("rtime"),

        /**
         * 按文件大小，降序
         */
        RSIZE("rsize"),

        /**
         * 按文件名称，降序
         */
        RNAME("rname");

        private final String val;
        SortBy(String val) {
            this.val = val;
        }

        public String getVal() {
            return val;
        }

        @Override
        public String toString() {
            return val;
        }
    }

    /**
     * 查询的路径
     */
    private String path;

    /**
     * 目录类别
     */
    private Location location = Location.MY_DOC;

    /**
     * true : 查询文件夹；false：查询单个文件
     */
    private Boolean isDir;

    /**
     * 分页查询，和page_size一起使用，表示请求页数，从1开始，默认值：0（表示不分页）
     */
    private Integer page;

    /**
     * 当page不等于0时有效，表示每页显示的文件内容大小
     */
    private Integer pageSize;

    /**
     * 排序
     */
    private SortBy sortBy = SortBy.RTIME;

    /**
     * 是否查询子目录
     */
    private Boolean queryChild;


    public static class Builder {
        private ListParameter listParameter = new ListParameter();

        public Builder path(String path) {
            listParameter.path = path;
            return this;
        }

        public Builder location(Location location) {
            listParameter.location = location;
            return this;
        }

        public Builder page(int page) {
            listParameter.page = page;
            return this;
        }

        public Builder pageSize(int pageSize) {
            listParameter.pageSize = pageSize;
            return this;
        }

        public Builder sortBy(SortBy sortBy) {
            listParameter.sortBy = sortBy;
            return this;
        }

        public Builder queryChild(boolean queryChild) {
            listParameter.queryChild = queryChild;
            return this;
        }

        public ListParameter build() {
            return listParameter;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

}
