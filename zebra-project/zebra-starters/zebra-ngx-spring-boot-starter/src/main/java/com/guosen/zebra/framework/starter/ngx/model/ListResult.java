package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

import java.util.List;

/**
 * 获取文件列表结果
 */
@Data
public class ListResult {
    /**
     * 总数
     */
    private int total;

    /**
     * 文件列表
     */
    private List<FileInfo> children;
}
