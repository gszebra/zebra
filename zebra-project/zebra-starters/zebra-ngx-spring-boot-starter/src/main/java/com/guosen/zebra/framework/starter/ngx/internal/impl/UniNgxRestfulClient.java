package com.guosen.zebra.framework.starter.ngx.internal.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.guosen.zebra.framework.basic.http.OKHttpClientUtil;
import com.guosen.zebra.framework.starter.ngx.exception.NgxException;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;



import java.util.Map;

/**
 * 安渡RESTful客户端
 */
@Slf4j
public final class UniNgxRestfulClient {
    private UniNgxRestfulClient(){}

    /**
     * JSON类型
     */
    public static final MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");

    /**
     * 二进制流类型
     */
    public static final MediaType APPLICATION_OCT_STREAM = MediaType.parse("application/octet-stream");

    /**
     * http协议客户端
     */
    private static final OkHttpClient HTTP_CLIENT = OKHttpClientUtil.getHttpClient();

    /**
     * https客户端，忽略证书
     */
    private static final OkHttpClient UNSAFE_HTTPS_CLIENT = OKHttpClientUtil.getUnsafeHttpsClient();

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static JsonNode get(String url, Map<String, String> headers, Map<String, String> parameters)
            throws NgxException {
        Request.Builder builder = new Request.Builder();

        setHeaders(headers, builder);

        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
        for(Map.Entry<String, String> parameter : parameters.entrySet()) {
            urlBuilder.addQueryParameter(parameter.getKey(), parameter.getValue());
        }

        builder.url(urlBuilder.build());

        return doInvoke(builder.build(), url);
    }

    public static JsonNode post(String url, Map<String, String> headers, Map<String, String> parameters)
            throws NgxException {
        String jsonParam = null;
        try {
            jsonParam = OBJECT_MAPPER.writeValueAsString(parameters);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        RequestBody body = RequestBody.create(JSON_TYPE, jsonParam);
        Request.Builder builder = new Request.Builder().url(url).post(body);

        setHeaders(headers, builder);

        Request request = builder.build();

        return doInvoke(request, url);
    }


    public static JsonNode postWithFile(String url, Map<String, String> headers, Map<String, String> parameters, byte[] fileContent)
            throws NgxException {


        Request.Builder builder = new Request.Builder();

        setHeaders(headers, builder);

        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
        for(Map.Entry<String, String> parameter : parameters.entrySet()) {
            urlBuilder.addQueryParameter(parameter.getKey(), parameter.getValue());
        }

        builder.url(urlBuilder.build());

        RequestBody body = RequestBody.create(APPLICATION_OCT_STREAM, fileContent);
        builder.post(body);

        Request request = builder.build();

        return doInvoke(request, url);
    }

    public static Object downloadFile(String url, Map<String, String> headers, Map<String, String> parameters) throws NgxException {
        Request.Builder builder = new Request.Builder();

        setHeaders(headers, builder);

        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
        for(Map.Entry<String, String> parameter : parameters.entrySet()) {
            urlBuilder.addQueryParameter(parameter.getKey(), parameter.getValue());
        }

        builder.url(urlBuilder.build());

        Object result = null;
        try (Response response = getHttpClient(url).newCall(builder.build()).execute();
             ResponseBody responseBody = response.body()) {
            String contentResult = response.header("Content-Result");
            if (StringUtils.equalsIgnoreCase("ERROR", contentResult)) {
                String respString = responseBody.string();
                if (StringUtils.isNotBlank(respString)) {
                    result = OBJECT_MAPPER.readTree(respString);
                }
            }
            else {
                result = responseBody.bytes();
            }
        } catch (Exception e) {
            log.error("Failed to invoke {}, error message : {}", url, e.getMessage(), e);
            throw new NgxException("500", e.getMessage());
        }

        return result;
    }

    private static void setHeaders(Map<String, String> headers, Request.Builder builder) {
        for (Map.Entry<String, String> header : headers.entrySet()) {
            builder.header(header.getKey(), header.getValue());
        }
    }

    private static JsonNode doInvoke(Request request, String url) throws NgxException {
        JsonNode result = null;
        try (Response response = getHttpClient(url).newCall(request).execute();
             ResponseBody responseBody = response.body()){
            if (responseBody != null) {
                String respString = responseBody.string();
                if (StringUtils.isNotBlank(respString)) {
                    result = OBJECT_MAPPER.readTree(respString);
                }
            }
        } catch (Exception e) {
            log.error("Failed to invoke {}, error message : {}", url, e.getMessage(), e);
            throw new NgxException("500", e.getMessage());
        }

        return result;
    }

    private static OkHttpClient getHttpClient(String url) {
        boolean isHttps = StringUtils.startsWithIgnoreCase(url, "https");
        return isHttps ? UNSAFE_HTTPS_CLIENT : HTTP_CLIENT;
    }
}
