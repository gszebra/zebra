package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

import java.util.List;

/**
 * 彻底删除文件（夹）请求参数
 */
@Data
public class DestroyParameter {

    /**
     * 文件或文件夹的唯一标识ID
     */
    private List<String> fsId;

    public static class Builder {
        private DestroyParameter destroyParameter = new DestroyParameter();

        public Builder fsId(List<String> fsId) {
            destroyParameter.fsId = fsId;
            return this;
        }

        public DestroyParameter build() {
            return destroyParameter;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
