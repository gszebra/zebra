package com.guosen.zebra.framework.starter.ngx.model;

/**
 * 目录类别
 */
public enum Location {
    /**
     * 我的文档
     */
    MY_DOC("1"),

    /**
     * 公共目录
     */
    COMMON_DIR("2");

    private final String val;
    Location(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    @Override
    public String toString() {
        return val;
    }
}
