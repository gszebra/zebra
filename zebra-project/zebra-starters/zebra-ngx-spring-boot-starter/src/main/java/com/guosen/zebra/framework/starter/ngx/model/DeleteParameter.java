package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

/**
 * 删除文件（夹）参数
 */
@Data
public class DeleteParameter {

    /**
     * 文件（夹）位置
     */
    private String path;

    /**
     * 目录类型
     */
    private Location location = Location.MY_DOC;

    /**
     * true:删除文件夹，false：删除文件。
     */
    private Boolean isDir;

    public static class Builder {
        private DeleteParameter deleteParameter = new DeleteParameter();

        public Builder path(String path) {
            deleteParameter.path = path;
            return this;
        }

        public Builder location(Location location) {
            deleteParameter.location = location;
            return this;
        }

        public Builder isDir(boolean isDir) {
            deleteParameter.isDir = isDir;
            return this;
        }

        public DeleteParameter build() {
            return deleteParameter;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
