package com.guosen.zebra.framework.starter.ngx.api;


import com.guosen.zebra.framework.starter.ngx.exception.NgxException;
import com.guosen.zebra.framework.starter.ngx.model.*;

/**
 * 安渡客户端接口
 */
public interface NgxClient {

    /**
     * 获取文件列表
     * @param listParameter 查询参数
     * @return  获取文件列表结果
     */
    ListResult list(ListParameter listParameter) throws NgxException;

    /**
     * 上传文件
     * @param uploadParameter   上传文件参数
     * @return  上传文件结果
     * @throws NgxException
     */
    UploadResult upload(UploadParameter uploadParameter) throws NgxException;

    /**
     * 下载文件
     * @param downloadParameter 下载文件参数
     * @return  下载文件结果
     * @throws NgxException
     */
    DownloadResult download(DownloadParameter downloadParameter) throws NgxException;


    /**
     * 创建目录
     * @param mkdirParameter    创建文件夹参数
     * @return  创建文件夹结果
     * @throws NgxException
     */
    MkdirResult mkdir(MkdirParameter mkdirParameter) throws NgxException;

    /**
     * 移动文件（夹）
     * @param moveParameter 移动文件（夹）参数
     * @return  移动文件结果
     * @throws NgxException
     */
    MoveResult move(MoveParameter moveParameter) throws NgxException;

    /**
     * 删除文件（夹）
     * @param deleteParameter   删除文件（夹）请求参数
     * @throws NgxException
     */
    void delete(DeleteParameter deleteParameter) throws NgxException;

    /**
     * 彻底删除文件（夹）
     * @param destroyParameter 彻底删除文件（夹）参数
     * @throws NgxException
     */
    void destroy(DestroyParameter destroyParameter) throws NgxException;
}
