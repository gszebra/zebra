package com.guosen.zebra.framework.starter.ngx.model;

import lombok.Data;

/**
 * 创建文件夹参数
 */
@Data
public class MkdirParameter {
    /**
     * 文件夹
     */
    private String path;

    /**
     * 位置类别
     */
    private Location location = Location.MY_DOC;

    public static class Builder {
        private MkdirParameter mkdirParameter = new MkdirParameter();

        public Builder path(String path) {
            mkdirParameter.path = path;
            return this;
        }

        public Builder location(Location location) {
            mkdirParameter.location = location;
            return this;
        }

        public MkdirParameter build() {
            return mkdirParameter;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

}
